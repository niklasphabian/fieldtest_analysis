import database
import matplotlib.pyplot as plt


periode ='winter'

startD = {'all':'2013-12-01', 'winter':'2015-11-22', 'summer':'2014-05-01'}
endD = {'all':'2015-10-01', 'winter':'2016-02-22','summer':'2014-10-01'}

start = startD[periode]
end = endD[periode]

temp_DB = database.FlowTemperatures(7)
temp_DB.setStart(start)
temp_DB.setEnd(end)

#temp_DB.getCHPRetTempTS()
temp_DB.getBoiRetTempTS()
#temp_DB.getBoiFlowTempTS()
temp = temp_DB.temperature

print(temp)

fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(7, 5))
temp.hist(axes=axes, normed=True)

print(temp.mean())

axes.set_ylim(0, 0.08)

axes.set_xlabel('Return temperature in deg C')
axes.set_ylabel('Frequency (Normalized number of occasions)')

plotFileName = 'TRet_.pdf'
plt.savefig(plotFileName)
plt.show()