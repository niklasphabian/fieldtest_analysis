import matplotlib.pyplot as plt
import datetime 
import matplotlib as mpl
import database
import pandas

class PingSeries:
    def __init__(self, boxnr, start_date, end_date):
        self.boxnr=boxnr       
        self.changes=[]
        self.changesDate=[]
        self.start_date = start_date
        self.end_date = end_date
    
    def init_db(self):
        self.db = database.SofieldPings(self.boxnr)
                    
    def get_series(self):        
        self.init_db()         
        self.db.setStart(self.start_date)
        self.db.setEnd(self.end_date)
        self.series = self.db.getTS()
        self.process_serie()
    
    def process_serie(self):        
        startCol = pandas.Series([False], [self.start_date], name = 'available')
        endCol = pandas.Series([False], [self.end_date], name = 'available')
        self.series = startCol.append(self.series).append(endCol)     
        self.series = self.series.resample(rule='H').first()
        self.series.fillna(False, inplace=True)
        self.findCrosses()     
                
    def findCrosses(self):    
        prevVal = False
        i = 0
        self.changes.append(self.series[0])   
        self.changesDate.append(self.series.index[0])
        for value in self.series:                        
            i=i+1;
            if prevVal != value:                
                self.changes.append(i)
                date = self.series.index[i-1]
                self.changesDate.append(datetime.datetime(date.year, date.month, date.day, date.hour, date.minute))
            prevVal=value
        self.changes.append(i)
        self.changesDate.append(self.series.index[-1])

class DataSeries(PingSeries):
    def init_db(self):
        self.db = database.SofieldAvailibility(self.boxnr)    
    
    def process_serie(self):       
        self.series.fillna(1.0, inplace=True) #Replace empty rows
        self.series = (self.series.resample(rule='Min').first()-self.series+1)        
        self.series.fillna(False, inplace=True)
        startCol = pandas.Series([True], [self.start_date], name = 'availible')
        endCol = pandas.Series([False], [self.end_date], name = 'availible')
        self.series = startCol.append(self.series).append(endCol)        
        self.series = self.series.resample(rule='H').first()
        self.series.fillna(False, inplace=True)
        self.findCrosses()

class Plot():
    def __init__(self, series):
        self.series = series
        
    def makeColormap(self):                
        cm=[]
        cm.append('#B0B0B0')        
        for i in range(2,len(self.series.changes)-2):
            if i%2 == 0 :
                cm.append('#E25C04')
            else:
                cm.append('#95c43d')
        cm.append('#B0B0B0')
        cmap = mpl.colors.ListedColormap(cm)
        cmap.set_over(color='k',alpha=None)
        cmap.set_under(color='k',alpha=None)
        return cmap
        
    def makeTicks(self):        
        ticks= self.series.changesDate
        dateprev= self.series.changesDate[0]
        i=0
        for date in self.series.changesDate:
            delta = date - dateprev
            if delta<datetime.timedelta(weeks=1):
                if i > 0:
                    ticks[i] = ' '                    
            i=i+1    
            dateprev=date
        return ticks

    def plot(self, ax):     
        cmap = self.makeColormap()
        bounds = self.series.changes
        norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
        cb2 = mpl.colorbar.ColorbarBase(ax, cmap=cmap,
                                     norm=norm,
                                     boundaries=[-1] + bounds + [bounds[-1]],
                                     extend='both',
                                     ticks=bounds, # optional
                                     spacing='proportional',
                                     orientation='horizontal')
        cb2.set_label('Availibility of Box # ' +str(self.series.boxnr))
        ticks = self.makeTicks()
        column_labels = ticks        
        ax.set_xticklabels(column_labels, minor=False,rotation=90)        
        ax.tick_params(labelsize=8)

def plotAll(start_date, end_date, ping_or_data):
    if ping_or_data =='ping':
        UsedClass = PingSeries
        outfilename = 'availability_ping.pdf'
    elif ping_or_data == 'data' :
        UsedClass = DataSeries
        outfilename = 'availability_data.pdf'
    fig = plt.figure(figsize=(20,60))
    for boxnr in range(0,13):   
        print(boxnr)    
        s = UsedClass(boxnr+1, start_date, end_date)        
        s.get_series()  
        ypos = 1-(boxnr+1)/14.0
        ax = fig.add_axes([0.1, ypos, 0.9, 0.02])
        ax.grid('on') 
        myplot = Plot(s)
        myplot.plot(ax)
    plt.title(outfilename.replace('.pdf', ''))      
    plt.savefig(outfilename)
    

def plotSingle(boxnr, start_date, end_date):
    fig, axes = plt.subplots(nrows=2, ncols=1, figsize=(20, 8))
    s = PingSeries(boxnr, start_date, end_date)
    #s = DataSeries(boxnr, start_date, end_date)
    s.get_series()
    myplot = Plot(s)
    myplot.plot(axes[0])
    s.series.plot(axes=axes[1])
    axes[1].set_ylim(-1,2)
    plt.tight_layout()
    plt.show()
    
start_date = datetime.datetime(2013,1,1)
end_date = datetime.datetime(2016,8,10) 
        
plotAll(start_date, end_date, 'ping')
plotAll(start_date, end_date, 'data')

#plotSingle(1, start_date, end_date)


