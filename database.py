import pyodbc
import datetime
import pandas
import pytz
import sqlalchemy
import pickle
import db_config

met = pytz.timezone('MET')
utc = pytz.timezone('UTC')
    

class SofieldDB:
    def __init__(self, plantNumber):
        self.makeCon()
        self.plantNumber = plantNumber
        self.dbTable = 'dp{pNum}'.format(pNum = plantNumber)
        self.getPlantName()
    
    def setStart(self, startDate):
        self.start = startDate
    
    def setEnd(self, endDate):
        self.end = endDate
    
    def makeCon(self):       
        constr= 'DRIVER=FreeTDS; \
            SERVER={host}; \
            DATABASE={db_name}; \
            UID={user}; \
            PWD={pwd}; \
            PORT=1433; \
            TDS_Version=8.0; \
            CHARSET=UTF8;'
            # FART // hdf:!3Lg;           
        constr = constr.format(host=db_config.host, db_name=db_config.db_name, user=db_config.user, pwd=db_config.pwd)
        self.conn = pyodbc.connect(constr)    
        self.cursor = self.conn.cursor()
        
    def makeSAEngine(self):
        con_string = 'mssql+pyodbc://{host}/{db_name}?driver=FreeTDS?port=1433?charset=utf8'
        con_string = con_string.format(host=db_config.host, db_name=db_config.db_name)
        self.engine = sqlalchemy.create_engine()
        self.saConn = self.engine.raw_connection()
        
    def getPlantName(self):
        query = 'SELECT city \
                FROM EiferDataHubDB.dbo.Buildings, EiferDataHubDB.dbo.Plants \
                WHERE EiferDataHubDB.dbo.Buildings.id = EiferDataHubDB.dbo.Plants.building_id \
                AND EiferDataHubDB.dbo.Plants.plantNumber = ? '
        self.cursor.execute(query, (self.plantNumber))
        plantName = self.cursor.fetchone()[0]
        self.plantName = plantName
        
   
class FlowTemperatures(SofieldDB):
    def getCHPRetTempTS(self):
        query = 'SELECT pointInTime, dk510_6 AS TRetCHP \
                 FROM PlantDataDB.dbo.{dbTable} \
                 WHERE pointInTime >= ? AND pointInTime <= ? \
                 AND dk510_6 < 100 \
                 AND dk110_4 > 0'.format(dbTable = self.dbTable)
        self.cursor.execute(query, (self.start, self.end,))
        self.data = self.cursor.fetchall()
        self.convertDataToPandas() 
    
    def getBoiRetTempTS(self):
        query = 'SELECT pointInTime, dk532_6 AS TRetCHP \
                 FROM PlantDataDB.dbo.{dbTable} \
                 WHERE pointInTime >= ? AND pointInTime <= ? \
                 AND dk532_6 < 100 \
                 AND dk532_3 > 0'.format(dbTable = self.dbTable)
        self.cursor.execute(query, (self.start, self.end,))
        self.data = self.cursor.fetchall()        
        self.convertDataToPandas() 
        
    def getBoiFlowTempTS(self):
        query = 'SELECT pointInTime, dk532_5 AS TRetCHP \
                 FROM PlantDataDB.dbo.{dbTable} \
                 WHERE pointInTime >= ? AND pointInTime <= ? \
                 AND dk532_5 < 100 \
                 AND dk532_3 > 0'.format(dbTable=self.dbTable)
        self.cursor.execute(query, (self.start, self.end,))
        self.data = self.cursor.fetchall()        
        self.convertDataToPandas()
    
    def convertDataToPandas(self):            
        time = []
        temperature = []
        for row in self.data:
            time.append(row[0])            
            temperature.append(row[1])            
        self.temperature = pandas.TimeSeries(temperature, time, name = 'TRetCHP')


class GasDemand(SofieldDB):
    def fetchData(self):
        query = 'SELECT pointInTime, dk200_2, dk220_2 \
                FROM PlantDataDB.dbo.{dbTable} \
                WHERE pointInTime >= ? AND pointInTime <= ? \
                ORDER BY pointInTime'.format(dbTable=self.dbTable)
        self.cursor.execute(query, (self.start, self.end))
        self.makeDataFrame(self.cursor.fetchall())
    
    def makeDataFrame(self, data):
        newData = []
        for row in data:
            newData.append(list(row))
        self.df = pandas.DataFrame(newData, columns=['timestamp', 'V_CHP', 'V_Boi'])
        self.df.drop_duplicates('timestamp', inplace=True, keep='first')
        self.df.set_index('timestamp', inplace=True)                
        self.addEnergy()
        self.addPower()
        
    def addEnergy(self):
        self.df['E_CHP'] =  self.df['V_CHP'] * 11.8
        self.df['E_Boi'] =  self.df['V_Boi'] * 11.8
        
    def addPower(self):
        p_chp = (self.df['E_CHP'].resample('15min').first().diff()*4).resample('1min').bfill()        
        self.df['P_CHP'] = p_chp
        p_boi = (self.df['E_Boi'].resample('15min').first().diff()*4).resample('1min').bfill()
        self.df['P_Boi'] = p_boi

class Electricity(SofieldDB):    
    
    def newest_elexp(self):
        query =     'SELECT TOP 1 pointInTime \
                    FROM PlantDataDB.dbo.{dbTable} \
                    WHERE dk190_4 IS NOT NULL \
                    ORDER BY pointInTime'.format(dbTable=self.dbTable)
        self.cursor.execute(query)
        return self.cursor.fetchone()[0]
    
    def fetchData(self):
        query = 'SELECT pointInTime, dk110_1, dk190_1, dk191_1, dk110_4/1000, dk190_4/1000, dk191_4/1000 \
                 FROM PlantDataDB.dbo.{dbTable} \
                 WHERE pointInTime >= ? AND pointInTime <= ? \
                 ORDER BY pointInTime'.format(dbTable=self.dbTable)
        self.cursor.execute(query, (self.start, self.end))
        self.makeDataFrame(self.cursor.fetchall())

    def makeDataFrame(self, data):
        newData = []        
        for row in data:
            newData.append(list(row))            
        self.df = pandas.DataFrame(newData,columns=['timestamp','E_CHP','E_Imp','E_Exp','P_CHP','P_Imp','P_Exp'])
        self.df.drop_duplicates('timestamp', inplace=True, keep='first')
        self.df.set_index('timestamp', inplace=True)        
        if self.plantNumber in [3,5,8,10]:            
            self.replace_export()
        self.add_selfcons()
        self.add_demand()           
        if self.plantNumber == 1:            
            self.df['E_Imp'][self.df['E_Imp'].index > datetime.datetime(2015,5,26,14)] += 6822.1
        self.remove_zeros()
            
    def remove_zeros(self):
        self.df['E_CHP'] = self.df['E_CHP'][self.df['E_CHP']>0]
        self.df['E_Exp'] = self.df['E_Exp'][self.df['E_Exp']>0]
        self.df['E_Imp'] = self.df['E_Imp'][self.df['E_Imp']>0]
    
    def extrapolate(self):
        pass             
        
    def pickleData(self):
        with open('dataframe.pickle', 'wb') as dumpfile:
            pickle.dump(self.df, dumpfile)
        
    def replace_export(self):
        self.df['P_Exp'] =  abs(self.df['P_Imp'] - abs(self.df['P_Imp']))/2
        self.df['P_Imp'] = (self.df['P_Imp'] + abs(self.df['P_Imp']))/2
    
    def add_selfcons(self):
        self.df['P_SC'] = self.df['P_CHP'] - self.df['P_Exp']
        self.df['P_SC'] = self.df['P_SC'][self.df['P_SC'] >= 0]
                
    def add_demand(self):
        self.df['P_Dem'] = self.df['P_CHP'] - self.df['P_Exp'] + self.df['P_Imp']
        self.df['P_Dem'] = self.df['P_Dem'][self.df['P_Dem'] >= 0]
        
    def totalProduction(self):
        return self.df['E_CHP'][-1]-self.df['E_CHP'][0]
    
    def totalDemand(self):
        print(self.df)
        prod = self.df['E_CHP'][-1]-self.df['E_CHP'][0]
        exp = self.df['E_Exp'][-1]-self.df['E_Exp'][0]        
        sc = prod - exp        
        imp = self.df['E_Imp'][-1]-self.df['E_Imp'][0]
        demand = sc + imp
        return demand

    def mean_power_by_hour_chp(self, hour):
        return  self.df['P_CHP'][self.df.index.hour==hour].mean()
        
    def mean_power_by_hour_import(self, hour):
        return  self.df['P_Imp'][self.df.index.hour==hour].mean()
    
    def mean_power_by_hour_export(self, hour):
        return  self.df['P_Exp'][self.df.index.hour==hour].mean()
    
    def mean_power_by_hour_selfcons(self, hour):
        return  self.df['P_SC'][self.df.index.hour==hour].mean()
    
    def mean_power_by_hour_demand(self, hour):
        return  self.df['P_Dem'][self.df.index.hour==hour].mean()
    
    def mean_power_by_day_chp(self, weekday):
        return  self.df['P_CHP'][self.df.index.weekday==weekday].mean()        

    def mean_power_by_day_import(self, weekday):
        return  self.df['P_Imp'][self.df.index.weekday==weekday].mean()
            
    def mean_power_by_day_export(self, weekday):                        
        return  self.df['P_Exp'][self.df.index.weekday==weekday].mean() 

    def mean_power_by_day_selfcons(self, weekday):
        return  self.df['P_SC'][self.df.index.weekday==weekday].mean() 
    
    def mean_power_by_day_demand(self, weekday):
        return  self.df['P_Dem'][self.df.index.weekday==weekday].mean()
                
    def mean_power_by_month_chp(self, month):
        return  self.df['P_CHP'][self.df.index.month==month].mean()        

    def mean_power_by_month_import(self, month):
        return  self.df['P_Imp'][self.df.index.month==month].mean()
            
    def mean_power_by_month_export(self, month):                        
        return  self.df['P_Exp'][self.df.index.month==month].mean()  

    def mean_power_by_month_selfcons(self, month):
        return  self.df['P_SC'][self.df.index.month==month].mean() 
    

class ElectricityMandelieu(Electricity):
    def __init__(self):
        self.makeCon()
        self.plantName = 'Mandelieu'
        
    def fetchData(self):
        query = 'SELECT dp11.pointInTime, dp12.dk110_1, dp11.dk101_1, dp12.dk110_4/1000,  dp11.dk101_4/1000 \
                 FROM PlantDataDB.dbo.dp11, PlantDataDB.dbo.dp12 \
                 WHERE dp11.pointInTime = dp12.pointInTime \
                 AND dp11.pointInTime >= ? AND dp11.pointInTime <= ? \
                 ORDER BY dp11.pointInTime'
        self.cursor.execute(query, (self.start, self.end,))
        self.makeDataFrame(self.cursor.fetchall())

    def makeDataFrame(self, data):
        newData = []        
        for row in data:
            newData.append(list(row))            
        self.df = pandas.DataFrame(newData,columns=['timestamp','E_CHP','E_Exp','P_CHP','P_Exp'])
        self.df['P_Dem'] = 0
        self.df['P_Imp'] = 0
        self.df['P_SC'] = 0
        self.df.drop_duplicates('timestamp', inplace=True, keep='first')
        self.df.set_index('timestamp', inplace=True)        
            
               
class Thermal(SofieldDB):             
    def fetchData(self):
        query = 'SELECT \
                    pointInTime, \
                    dk561_1 AS E_ThSH, \
                    dk570_1 AS E_ThDHW, \
                    dk510_1 AS E_ThCHP, \
                    dk532_1 AS E_ThBoi, \
                    dk561_4/1000 AS P_ThSH, \
                    (dk570_5-dk570_6)*dk570_3/3600*4.179 AS P_ThDHW, \
                    (dk510_5-dk510_6)*dk510_3/3600*4.179 AS P_ThCHP, \
                    (dk532_5-dk532_6)*dk532_3/3600*4.179 AS P_ThBoi \
                 FROM PlantDataDB.dbo.{dbTable} \
                 WHERE pointInTime >= ? AND pointInTime < ? \
                 ORDER BY pointInTime'.format(dbTable=self.dbTable)        
        self.cursor.execute(query, (self.start, self.end,))
        self.makeDataFrame(self.cursor.fetchall())
        self.clean_zeros_from_accumulative_counters()
        if self.plantNumber in [1,2,3,4,5,6,9,10]:
            self.replace_SH()
        self.clean_negative_values()
        #self.clean_upper_limits()        
        
    def clean_zeros_from_accumulative_counters(self):
        self.df['E_SH'] = self.df['E_SH'][self.df['E_SH'] > 0]
        self.df['E_DHW'] = self.df['E_DHW'][self.df['E_DHW'] > 0]
        self.df['E_CHP'] = self.df['E_CHP'][self.df['E_CHP'] > 0]
        self.df['E_Boi'] = self.df['E_Boi'][self.df['E_Boi'] > 0]         
    
    def clean_negative_values(self):
        self.df = self.df[self.df>=0]        
    
    def clean_upper_limits(self):
        self.df = self.df[self.df<1000]
        
    def makeDataFrame(self, data):
        newData = []        
        for row in data:
            newData.append(list(row))            
        self.df = pandas.DataFrame(newData,columns=['timestamp','E_SH','E_DHW','E_CHP','E_Boi','P_SH','P_DHW','P_CHP','P_Boi'])
        self.df.drop_duplicates('timestamp', inplace=True, keep='first')
        self.df.set_index('timestamp', inplace=True)        

    def split_CHP_and_Boiler(self):        
        self.df['P_Boi'] = self.df['P_CHP'][self.df['P_CHP'] > 5]
        self.df['P_Boi'] = self.df['P_Boi'] - 5
        self.df['P_Boi'].fillna(0, inplace=True) 
        self.df['P_CHP'] = self.df['P_CHP'] - self.df['P_Boi'] 
                
    def replace_SH(self):
        sh = (self.df['E_SH'].resample('15min').first().diff()*4).resample('1min').bfill()        
        self.df['P_SH'] = sh
    
    def totalSHDemand(self):
        return self.df['E_SH'][-1]-self.df['E_SH'][0]                    
        
    def totalDHWDemand(self):
        return self.df['E_DHW'][-1]-self.df['E_DHW'][0]
        
    def mean_power_by_hour_SH(self, hour):
        return self.df['P_SH'][self.df.index.hour==hour].mean()
    
    def mean_power_by_hour_DHW(self, hour):
        return self.df['P_DHW'][self.df.index.hour==hour].mean()
            
    def mean_power_by_hour_Boi(self, hour):
        return self.df['P_Boi'][self.df.index.hour==hour].mean()
         
    def mean_power_by_hour_CHP(self, hour):
        return self.df['P_CHP'][self.df.index.hour==hour].mean()
        
    def mean_power_by_day_SH(self, day):
        return self.df['P_SH'][self.df.index.weekday==day].mean()
                
    def mean_power_by_day_DHW(self, day):
        return self.df['P_DHW'][self.df.index.weekday==day].mean()
    
    def mean_power_by_day_Boi(self, day):
        return self.df['P_Boi'][self.df.index.weekday==day].mean()
    
    def mean_power_by_day_CHP(self, day):
        return self.df['P_CHP'][self.df.index.weekday==day].mean()   
       
    def mean_power_by_month_SH(self, month):
        return self.df['P_SH'][self.df.index.month==month].mean()
        
    def mean_power_by_month_DHW(self, month):
        return self.df['P_DHW'][self.df.index.month==month].mean()        
    
    def mean_power_by_month_CHP(self, month):
        return self.df['P_CHP'][self.df.index.month==month].mean()
    
    def mean_power_by_month_Boi(self, month):
        return self.df['P_Boi'][self.df.index.month==month].mean()
    
class ThermalMandelieu(Thermal):
    def __init__(self):
        self.makeCon()
        self.plantName = 'Mandelieu'
        
    def fetchData(self):
            query = "SELECT \
                        PlantDataDB.dbo.dp11.pointInTime, \
                        CASE WHEN dp11.pointInTime > '2015-04-09 00:00:00' THEN dp11.dk561_1/1000 ELSE dp12.dk561_1/1000 + dp11.dk510_1/1000 END AS E_ThSH, \
                        (dp11.dk570_1/1000) AS E_ThDHW, \
                        (dp11.dk510_1/1000 + dp11.dk562_1/1000/1000) AS E_ThCHP, \
                        (dp11.dk532_1/1000 + dp11.dk563_1/1000/1000) AS E_ThBoi, \
                        (dp12.dk570_1/1000) AS E_ThSol, \
                        (dp12.dk510_1/1000) AS E_ThSolGluc, \
                        CASE WHEN dp11.pointInTime > '2015-04-10 00:00:00' THEN dp11.dk561_4/1000 ELSE dp12.dk561_4/1000 + dp11.dk510_4/1000 END AS P_ThSH, \
                        (dp11.dk570_4/1000) AS P_ThDHW, \
                        CASE WHEN dp11.pointInTime > '2015-02-06 00:00:00' THEN dp11.dk562_4/1000 + dp11.dk510_4/1000 ELSE dp11.dk510_4/1000 END AS P_ThCHP, \
                        CASE WHEN dp11.pointInTime > '2015-02-06 00:00:00' THEN dp11.dk532_4/1000 + dp11.dk563_4/1000 ELSE dp12.dk561_4/1000 + dp12.dk562_4/1000 END AS P_ThBoi, \
                        (dp12.dk570_4/1000) AS P_ThSol, \
                        (dp12.dk510_4/1000) AS P_ThSolGluc \
                    FROM PlantDataDB.dbo.dp11, PlantDataDB.dbo.dp12 \
                    WHERE \
                        PlantDataDB.dbo.dp11.pointInTime >= ? AND \
                        PlantDataDB.dbo.dp11.pointInTime <= ? AND \
                        PlantDataDB.dbo.dp11.pointInTime = PlantDataDB.dbo.dp12.pointInTime \
                    ORDER BY pointInTime ASC"
            self.cursor.execute(query, (self.start, self.end,))
            self.makeDataFrame(self.cursor.fetchall())
            self.clean_zeros_from_accumulative_counters()
            self.clean_negative_values()
            self.clean_upper_limits()
    
    def clean_upper_limits(self):
        self.df['P_Sol'] = self.df['P_Sol'][self.df['P_Sol'] < 100]
        self.df['P_SH'] = self.df['P_SH'][self.df['P_SH'] < 100]
        self.df['P_Boi'] = self.df['P_Boi'][self.df['P_Boi'] < 100]
        self.df['P_DHW'] = self.df['P_DHW'][self.df['P_DHW'] < 1000]
        self.df['P_SolGluc'] = self.df['P_SolGluc'][self.df['P_SolGluc'] < 100]
          
            
    def makeDataFrame(self, data):
        newData = []        
        for row in data:
            newData.append(list(row))            
        self.df = pandas.DataFrame(newData,columns=['timestamp','E_SH','E_DHW','E_CHP','E_Boi','E_Sol','E_SolGluc','P_SH','P_DHW','P_CHP','P_Boi','P_Sol', 'P_SolGluc'])
        self.df.drop_duplicates('timestamp', inplace=True, keep='first')
        self.df.set_index('timestamp', inplace=True)
        
    def mean_power_by_hour_Sol(self, hour):
        return self.df['P_Sol'][self.df.index.hour==hour].mean()

    def mean_power_by_day_Sol(self, day):
        return self.df['P_Sol'][self.df.index.weekday==day].mean()
    
    def mean_power_by_month_Sol(self, month):
        return self.df['P_Sol'][self.df.index.month==month].mean()


class HeatVsTemp(SofieldDB):
          
    def getHeatVsTemp(self):      
        query = 'SELECT pointintime, dk390_5 AS AmbAirTemp, dk561_4 AS QSH \
                 FROM PlantDataDB.dbo.{dbTable} \
                 WHERE pointInTime >= ? AND pointInTime <= ? \
                 order by pointintime'.format(dbTable = self.dbTable)
        self.cursor.execute(query, (self.start, self.end,))
        data = self.cursor.fetchall()        
        return self.convertDataToPandas(data)     
    
    def convertDataToPandas(self, data):
        timestamp = []            
        temperature= []
        heat = []
        for row in data:
            timestamp.append(row[0])
            temperature.append(row[1])            
            heat.append(row[2])
        heat = pandas.Series(heat).diff()*1000*36
        heat /= 1000                 
        data = {'heat': heat.values, 'temp':temperature}  
        heatVSTemp = pandas.DataFrame(data, index=timestamp)
        return heatVSTemp 
    
    
class SofieldPings(SofieldDB):
    def getTS(self):
        query = 'SELECT \
                TimeStamp, \
                Availible \
                FROM PlantDataDB.dbo.availibility \
                WHERE PlantNumber = ? \
                AND TimeStamp >= ? \
                AND TimeStamp <= ? \
                ORDER BY TimeStamp '
        self.cursor.execute(query, (self.plantNumber, self.start, self.end))
        data = self.cursor.fetchall()        
        return self.convertDataToPandas(data) 

    def convertDataToPandas(self, data):
        time = []
        availible = []        
        for row in data:
            time.append(datetime.datetime(row[0].year,row[0].month, row[0].day, row[0].hour, row[0].minute))            
            availible.append(row[1])            
        availible = pandas.Series(availible, time, name = 'availible')
        return availible         
      
    def getLatestTimeStamp(self, plantNumber):
        query = 'SELECT TimeStamp FROM PlantDataDB.dbo.availibility WHERE PlantNumber = ? AND Availible = 1 ORDER by TimeStamp DESC'        
        self.cursor.execute(query, (plantNumber))
        ret = self.cursor.fetchall()
        return ret[0][0]
    
class SofieldAvailibility(SofieldPings):
    def getTS(self):
        query = 'SELECT \
                pointInTime, \
                dk570_1 \
                FROM PlantDataDB.dbo.{} \
                WHERE pointInTime >= ? \
                AND pointInTime <= ? \
                ORDER BY pointInTime '.format(self.dbTable)    
        self.cursor.execute(query, (self.start, self.end))
        data = self.cursor.fetchall()        
        return self.convertDataToPandas(data)
