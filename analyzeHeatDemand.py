import database
import datetime
import plots
import matplotlib.pyplot as plt

class HeatDemandAnalyzer():
    def __init__(self, plantNumber):
        self.plantNumber = plantNumber
        #self.db = database.Thermal(self.plantNumber)
        self.db = database.ThermalMandelieu()
        self.plantName = self.db.plantName
        startDate = datetime.datetime(2014,1,1)
        endDate = datetime.datetime(2015,11,30)                
        self.db.setStart(startDate)
        self.db.setEnd(endDate)       
        self.db.fetchData()
        #self.db.split_CHP_and_Boiler()        
        self.canvas = plots.ThreePlotPage()
        self.canvas.makeTitle(self.plantNumber, self.plantName, startDate, endDate)        
    
    def plotTS(self):
        self.tsPlot = plots.TSPlot(self.plantNumber)
        timeseries = self.db.getDHWTS()
        self.tsPlot.plotTS(timeseries)
        self.tsPlot.save('TS')        
        
    def plotHour(self):
        avgSH = []        
        avgDHW = []
        avgBoi = []
        avgCHP = []
        avgSol = []
        for hour in range(0,24):
            avgDHW.append(self.db.mean_power_by_hour_DHW(hour))
            avgSH.append(self.db.mean_power_by_hour_SH(hour))
            avgCHP.append(self.db.mean_power_by_hour_CHP(hour))            
            avgBoi.append(self.db.mean_power_by_hour_Boi(hour))     
            avgSol.append(self.db.mean_power_by_hour_Sol(hour))       
        hourPlot = plots.HourPlot(self.canvas.axes[0])
        hourPlot.plot(avgDHW)
        hourPlot.plot(avgSH)        
        hourPlot.plot(avgCHP)
        hourPlot.plot(avgBoi)
        hourPlot.plot(avgSol)         
        hourPlot.setLegend(['DHW Demand', 'SH Demand',  'CHP',  'Aux. Boiler', 'Sol'])   
                                             
    def plotDays(self):
        avgSH = []
        avgDHW = []
        avgBoi = []
        avgCHP = []
        avgSol = []
        days = [0,1,2,3,4,5,6]
        for day in days:
            avgDHW.append(self.db.mean_power_by_day_DHW(day)*24)                        
            avgSH.append(self.db.mean_power_by_day_SH(day)*24)
            avgCHP.append(self.db.mean_power_by_day_CHP(day)*24)    
            avgBoi.append(self.db.mean_power_by_day_Boi(day)*24)
            avgSol.append(self.db.mean_power_by_day_Sol(day)*24)
        dayPlot = plots.DayPlot(self.canvas.axes[1])
        dayPlot.plotStacked(avgDHW, avgSH, legend = ['DHW Demand', 'SH Demand'])   
        #dayPlot.plotStacked(avgCHP, avgBoi, legend = ['CHP', 'Aux. Boiler'])
        dayPlot.plotTrippleStacked(avgCHP, avgBoi, avgSol, legend = ['CHP', 'Aux. Boiler', 'Sol'])        
        
    def plotMonths(self):        
        avgSH = []
        avgDHW = []
        avgBoi = []
        avgCHP = []
        avgSol = []
        months = range(1,13)
        for month in months:            
            avgDHW.append(self.db.mean_power_by_month_DHW(month)*24)            
            avgSH.append(self.db.mean_power_by_month_SH(month)*24)
            avgCHP.append(self.db.mean_power_by_month_CHP(month)*24)
            avgBoi.append(self.db.mean_power_by_month_Boi(month)*24)
            avgSol.append(self.db.mean_power_by_month_Sol(month)*24)                
        monthPlot = plots.MonthPlot(self.canvas.axes[2])        
        monthPlot.plotStacked(avgDHW, avgSH, legend = ['DHW Demand', 'SH Demand'])
        #monthPlot.plotStacked(avgCHP, avgBoi, legend = ['CHP', 'Aux. Boiler'])
        monthPlot.plotTrippleStacked(avgCHP, avgBoi, avgSol, legend = ['CHP', 'Aux. Boiler', 'Sol'])
       
        
        
        
    def saveFig(self):
        plotFileName = 'HeatDemand_{plantNum}.pdf'.format(plantNum = self.plantNumber)
        plt.savefig(plotFileName)  
    
class TotalHeatAnalyzer():
    def __init__(self, plantNumber):
        self.plantNumber = plantNumber
        self.db = database.Thermal(self.plantNumber)
        self.plantName = self.db.plantName
        startDate = self.db.earlierstEntry()+datetime.timedelta(days=10)       
        endDate = startDate + datetime.timedelta(days=365)
        self.timeSpan = endDate-startDate
        self.db.setStart(startDate)
        self.db.setEnd(endDate)
        print(startDate)
        print(endDate)
    
    def getTotalDemand(self):
        avSH = round(self.db.getTotalSHDemand()/self.timeSpan.total_seconds()*60*60*8760/1000, 2)
        avDHW = round(self.db.getTotalDHWDemand()/self.timeSpan.total_seconds()*60*60*8760/1000, 2)         
        print('{pName}; {avSH}; {avDHW} '.format(pName=self.plantName, avSH=avSH, avDHW=avDHW))
        
def tsAnalysis(site):
    myAnalyzer = HeatDemandAnalyzer(site)
    #myAnalyzer.plotTS()
    myAnalyzer.plotHour()
    myAnalyzer.plotDays()
    myAnalyzer.plotMonths()
    myAnalyzer.saveFig()
    
def absAnalysis():
    for plantNum in range(1, 13):
        myTotAnalyzer = TotalHeatAnalyzer(plantNum)
        myTotAnalyzer.getTotalDemand()
    
#absAnalysis()
tsAnalysis(11)
