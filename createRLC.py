import database
import datetime
import matplotlib.pyplot as plt
import numpy
from sklearn.cluster import KMeans
import pandas

class DayData():        
    def __init__(self):
        plantNumber = 7
        self.sfDatabase = database.Thermal(plantNumber)        
        #self.sfDatabase = database.SQLDB()
        
    def downloadDay(self, date):     
        self.sfDatabase.setStart(date)
        self.sfDatabase.setEnd(date + datetime.timedelta(days=1))              
        self.sfDatabase.fetchData()
        #self.timeSeries = self.sfDatabase.getDHWTS() + self.sfDatabase.getSHTS()
        self.timeSeries = self.sfDatabase.df['P_SH']        
        #self.timeSeries = self.sfDatabase.getDHWTS()        
            
    def resample(self):        
        self.timeSeries = self.timeSeries.resample('1H', how='mean')        
    
    def replaceNan(self):
        self.timeSeries = self.timeSeries.interpolate(method='time')
        self.timeSeries = self.timeSeries.fillna(method='ffill')
        self.timeSeries = self.timeSeries.fillna(method='bfill')                     

class DayDataArray():
    def setStartDate(self, startDate):
        self.startDate = startDate
        
    def setEndDate(self, endDate):
        self.endDate = endDate
        
    def downloadArray(self):
        date = self.startDate
        self.tsArray = []
        self.tsArrayOriginal = []
        myDayData = DayData()
        
        while date < self.endDate:
            myDayData.downloadDay(date)            
            if len(myDayData.timeSeries) == 1440:
                myDayData.replaceNan()
                self.tsArrayOriginal.append(myDayData.timeSeries)                        
                #myDayData.resample()            
                self.tsArray.append(myDayData.timeSeries)                
            date = date + datetime.timedelta(days=1)    
        
    def glueTS(self):
        fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(7, 5))
        self.originalTS = pandas.Series()             
        for ts in self.tsArrayOriginal:
            self.originalTS = self.originalTS.append(ts)    
        self.originalTS.plot(ax=axes, color='k', linestyle='--')
        
    def plotOriginals(self):
        fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(7, 5))
        axes.grid('on')
        for series in self.tsArray:
            axes.plot(series, 'y')        
        axes.set_xlabel('Time')
        axes.set_ylabel('Power in kW')        
        axes.set_xlim(0, len(self.tsArray[0]-1))
        #axes.set_ylim(0, 8)
        plt.savefig('RLC/Originals.pdf')
                
    def makeKMeans(self, nCluster):
        self.kmeans = KMeans(n_clusters=nCluster)
        self.kmeans.fit(self.tsArray)        
        
    def findCenters(self):
        distance = self.kmeans.transform(self.tsArray)
        self.centers = []
        for col in range(distance.shape[1]):
            ar = []
            for sample in distance:
                ar.append(sample[col])            
            self.centers.append(ar.index(min(ar)))
        self.centers
                           
    def makeClusterTS(self):        
        colorMap = ['r','b','g','y','c','m','k','r','b','g','y','c','m','k','r','b','g','y','c','m','k','r','b','g','y','c','m','k']
        self.clusterTS = pandas.Series()
        n = 0        
        for centerIDX in self.kmeans.labels_:
            timeStamps = self.tsArrayOriginal[n].index            
            values = self.tsArrayOriginal[self.centers[centerIDX]].values            
            ts = pandas.Series(values, timeStamps)
            ts.plot(color=colorMap[centerIDX], linewidth=1)
            self.clusterTS = self.clusterTS.append(ts)                    
            n = n + 1
        self.clusterTS.plot(linewidth=0)           
        plt.savefig('RLC/LoadCurve.pdf')
            
    def printCenterInfo(self):
        fig, axes = plt.subplots(nrows=len(self.centers), ncols=1, figsize=(15, 20))
        fig.suptitle('Representative load curves', fontsize=20)
        n = 0
        for center in self.centers:                        
            date = self.tsArray[center].index[0]
            if self.tsArray[center].index[0].weekday() == 7 or self.tsArray[center].index[0].weekday() == 1 :
                weekday = 'WE'
            else:
                weekday = 'WD'                
            demand =  round(self.tsArray[center].sum()/60)
            noa = sum(self.kmeans.labels_ == self.centers.index(center))
            self.tsArray[center].plot(ax=axes[n])
            axes[n].set_ylabel('Demand in kW')
            axes[n].grid('on')
            axes[n].set_title('{date} ({weekday}). Demand: {demand} kWh. Noa: {noa}'.format(date=date,weekday=weekday,demand=demand,noa=noa))            
            n=n+1
        plt.savefig('RLC/RLCOverview.pdf')
        
    def writeCenters(self):        
        n = 0
        for center in self.centers:
            n = n + 1
            fname='RLC/RLC_{n}.csv'.format(n=n)            
            with open(fname, 'w') as outfile:                
                self.tsArray[center].to_csv(outfile)
                
startDate = datetime.datetime(2014,9,1)
endDate = datetime.datetime(2015,8,30)
            
myDataArray = DayDataArray()
myDataArray.setStartDate(startDate)
myDataArray.setEndDate(endDate)

myDataArray.downloadArray()
myDataArray.plotOriginals()

myDataArray.glueTS()
myDataArray.makeKMeans(4)
myDataArray.findCenters()
myDataArray.makeClusterTS()

myDataArray.printCenterInfo()
myDataArray.writeCenters()

print('Total Cons Original: {}'.format(sum(myDataArray.originalTS)/60))
print('Total Cons Synthetic: {}'.format(sum(myDataArray.clusterTS)/60))
print('Difference: {}'.format(sum(myDataArray.clusterTS-myDataArray.originalTS)/60))
print('Absolute difference: {}'.format(sum(abs(myDataArray.clusterTS-myDataArray.originalTS))/60))
print('Corrcoeff: {}'.format(numpy.corrcoef(myDataArray.originalTS, myDataArray.clusterTS)[0][1]))


