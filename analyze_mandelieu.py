import datetime
import matplotlib
import pandas
import pymssql
import os

class Report():
    
    
    def __init__(self, eUser, ePass):
        
        #Need for processing:
        self.lConn = pymssql.connect("eifero249.eifer.uni-karlsruhe.de", eUser, ePass, "PlantDataDB")
        self.lExecutor = self.lConn.cursor()
        self.lTexte = "Yes"
        self.lDataHealth = ""
        self.autosave = False
        self.savePath = ""
        
        #Help on SQL
        self.ptInTime = "PlantDataDB.dbo.dp11.pointInTime"
        self.FROM = "PlantDataDB.dbo.dp11, PlantDataDB.dbo.dp12"
        self.WHEREand = "PlantDataDB.dbo.dp11.pointInTime = PlantDataDB.dbo.dp12.pointInTime"
        self.ORDERBY = "PlantDataDB.dbo.dp11.pointInTime DESC"
        
        #List of sensors:
        self.lSensor_Energy = "_1"
        self.lSensor_TotVolume = "_2"
        self.lSensor_MassFlow = "_3"
        self.lSensor_Power = "_4"
        self.lSensor_TempFlow = "_5"
        self.lSensor_TempReturn = "_6"
        
        self.lSensorGasBoil1 = "dp11.dk220_2"
        self.lSensorGasBoil2 = "dp11.dk201_2"
        self.lSensorGasCHP = "dp11.dk200_2"
        
        self.lSensorExtTemp = "dp11.dk390_5"
        
        self.lSensorSolarGross = "dp12.dk510"
        self.lSensorDHWdemand = "dp11.dk570"
        self.lSensorSolarNet = "dp11.dk570"
        self.lSensorRecirculation = "dp12.dk563"
        self.lSensorBoiler1 = "dp11.dk532"
        self.lSensorBoiler2 = "dp11.dk563"
        self.lSensorCHPtoDHW = "dp11.dk562"
        self.lSensorSHtoDHW = "dp12.dk562"
        self.lSensorSHdemand = "dp11.dk561"
        self.lSensorBoilerToSH = "dp12.dk561"
        self.lSensorCHPtoSH = "dp11.dk510"
        
        self.lSensorCHPconsumption = "dp12.dk100"
        self.lSensorCHPnetProd = "dp12.dk110"
        self.lSensorElecExported = "dp11.dk101"
        
        #List of parameters:
        self.gasPCS = 11.5 #kWh/m3  (from wikipedia)
        self.solarRadiation = [96.9, 120, 169, 176, 197, 205, 221, 214, 181, 144, 101, 91.2] #kWh/m2   monthly radiation [jan, feb, mar, apr, may, jun, jul, aug, sep, oct, nov, dec] from pvgis
        self.panelArea = 4.5 #m2
        
    def endClass(self):
        self.lConn.close()
        print("")
        print("FINISH")
    
    def resetStringAttribute(self):
        self.lDataHealth = ""
        self.lTexte = "Yes"
    
    
    
    def createReport(self, eDateBegin, eDateEnd, eSavePath):
        
        self.autosave = True
        self.savePath = eSavePath
        
        #os.mkdir(self.savePath)
        self.utilities_CreatingAndWritingTextFile(self.savePath + "\\Summary.txt", "Report automatically generated" + chr(10))
        
        #PARAMETER :
        self.get_HeatProduction_Console(eDateBegin, eDateEnd)
        self.get_HeatConsumption_Console(eDateBegin, eDateEnd)
        self.get_HeatLosses_Console(eDateBegin, eDateEnd)
        self.get_ElecBalance_Console(eDateBegin, eDateEnd)
        self.get_Efficiency_Console(eDateBegin, eDateEnd)
        #self.get_All_Console(eDateBegin, eDateEnd)
        
        #PLOT PIE :
        self.plotPie_GrossProduction(eDateBegin, eDateEnd)
        self.plotPie_EnergyRepartition(eDateBegin, eDateEnd)
        self.plotPie_HeatDemand(eDateBegin, eDateEnd)
        self.plotPie_HeatForDHW(eDateBegin, eDateEnd)
        self.plotPie_HeatForSH(eDateBegin, eDateEnd)
        self.plotPie_Losses(eDateBegin, eDateEnd)
        self.plotPie_AllEnergy(eDateBegin, eDateEnd)
        
        #PLOT LINE CHART
        self.plotLineChart_Elec_AvgDays(eDateBegin, eDateEnd)
        self.plotLineChart_Elec_AvgOnWeekDays(eDateBegin, eDateEnd)
        self.plotLineChart_ElecAvgOnMonth(eDateBegin, eDateEnd)
        self.plotLineChart_ElecAvgOnOneDay(eDateBegin, eDateEnd)
        self.plotLineChart_HeatDemand_AvgDays(eDateBegin, eDateEnd)
        self.plotLineChart_HeatDemand_AvgOnMonth(eDateBegin, eDateEnd)
        self.plotLineChart_HeatDemand_AvgOnOneDay(eDateBegin, eDateEnd)
        self.plotLineChart_HeatDemand_AvgOnWeekDay(eDateBegin, eDateEnd)
        self.plotLineChart_SolarProductionAvgDays(eDateBegin, eDateEnd)
        self.plotLineChart_SolarProductionAvgOnMonth(eDateBegin, eDateEnd)
        self.plotLineChart_SolarProductionAvgOnOneDay(eDateBegin, eDateEnd)
        self.plotLineChart_CHP_DeltaT(eDateBegin, eDateEnd)
        
        #PLOT SCATTER
        self.plotScatter_Sigmoid_SH_AvgOnDays(eDateBegin, eDateEnd, 10)
    
    
    
    def executeQuery(self, eQuery):
        self.lExecutor.execute(eQuery)
        result = []
        result.append(self.lExecutor.fetchall())
        return result
         
    def executeListOfQuery(self, eStringList):
        tabResult = []
        for i in range(0,len(eStringList)):
            self.lExecutor.execute(eStringList[i])
            tabResult.append(self.lExecutor.fetchall())
        return tabResult
    
    
    
    def beCarefulOfDataHealth(self):
        if self.lDataHealth != "":
            print("")
            print(" /!\ Be careful, Data may not be good for the following parameters (and all calculation which needs them) : ")
            print(self.lDataHealth)
        
        
        
    def utilities_transposeArray(self, eArray):
        rowNumber = len(eArray[1]) #That supposed that all row have the same number of column 
        colNumber = len(eArray)
        result = []
        
        for i in range(0, rowNumber):
            result.append([])
            for j in range(0, colNumber):
                result[i].append(0)
        
        for i in range(0,len(eArray)):
            for j in range(0,len(eArray[i])):
                result[j][i] = eArray[i][j]
        
        return result
    
    def utilities_GetNumberOfDays(self, eDateBegin, eDateEnd):
        delta = eDateEnd - eDateBegin
        return delta.days 
    
    def utilities_GetDayOfWeek(self, eDate):
        referenceDate = datetime.datetime(2014,1,6) #it is a Monday
        NbDay = self.utilities_GetNumberOfDays(referenceDate, eDate)
        return NbDay % 7 
    
    def utilities_GetMonth(self, eDate):
        return eDate.month - 1
    
    def utilities_GetMinuteOfDay(self, eDate):
        lhour = eDate.hour
        lminute = eDate.minute
        return (lhour * 60) + lminute 
    
    def utilities_GenericPlot(self, eBox, eCounter, eDateBegin, eDateEnd):
        query = 'SELECT PlantDataDB.dbo.dp11.pointInTime, dp'+ eBox +'.dk' + eCounter + ' FROM PlantDataDB.dbo.dp11, PlantDataDB.dbo.dp12 WHERE dp11.pointInTime > \'' + eDateBegin.strftime("%Y-%m-%d %H:%M:%S") + '\' AND dp11.pointInTime < \'' + eDateEnd.strftime("%Y-%m-%d %H:%M:%S") + '\' AND dp11.pointInTime = dp12.pointInTime' #.format(Box=eBox,Counter=eCounter)
        result = self.executeQuery(query)
        
        tabDate = []
        tabValue = []
        
        for i in range(0, len(result[0])):
            tabDate.append(result[0][i][0])
            tabValue.append(result[0][i][1])
        
        graph = pandas.DataFrame(tabValue, columns=['dp' + eBox + '.dk' + eCounter])#, name="DHW demand, average on days")
        graph['Date'] = pandas.Series(tabDate)
        graph.plot(x='Date', y= 'dp' + eBox + '.dk' + eCounter)
        matplotlib.pyplot.show()
    
    def utilities_GenericPlots(self, eSensors, eDateBegin, eDateEnd):
        #Don't work yet
        
        string = "PlantDataDB.dbo.dp11.pointInTime"
        
        for i in eSensors:
            string = string + ", " + i
        
        query = 'SELECT ' + string + ' FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' > \'' + eDateBegin.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.ptInTime + ' < \'' + eDateEnd.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY #.format(Box=eBox,Counter=eCounter)'
        result = self.executeQuery(query)
        
        tab_Date = []
        tabValues = []
        numberOfSensors = len(eSensors)
        
        for i in range(0, numberOfSensors):
            tabValues.append([])
        
        for i in range(0, len(result[0])):
            tab_Date.append(result[0][i][0])
            
            for j in range(1, numberOfSensors):
                tabValues[j].append(result[0][i][j])
        
        #print(tabValues)
        
        graph = pandas.DataFrame(self.utilities_transposeArray(tabValues), columns= eSensors)#, name="DHW demand, average on days")
        graph['Date'] = pandas.Series(tab_Date)
        graph.plot(x='Date', y=eSensors)
        matplotlib.pyplot.show()
    
    def utilities_CreatingAndWritingTextFile(self, eAddress, eText):
        with open(eAddress, "a") as lFile:
            lFile.write(eText)
        lFile.close()
        
    def utilities_AddingOnTextFile(self, eAddress, eText):
        with open(eAddress, "r") as lFile:
            oldText = lFile.read()
        lFile.close()
        with open(eAddress, "w") as lFile2:
            lFile2.write(oldText + eText)
        lFile2.close()
 
 
    
    def qry_GetAllDataBase(self, eStartDate, eEndDate):
        query = 'SELECT * FROM ' + self.FROM + ' WHERE ' + self.ptInTime + '>' + eStartDate + ' AND  ' + self.ptInTime + '<' + eEndDate + ' ' + self.WHEREand + ' ' + self.ORDERBY
        return self.executeQuery(query)
   
    def qry_EnergySolarPanels(self, eDateBegin, eDateEnd):
        query1 = 'SELECT ' + self.lSensorSolarGross + self.lSensor_Energy + ' FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' = \'' + eDateBegin.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY 
        query2 = 'SELECT ' + self.lSensorSolarGross + self.lSensor_Energy + ' FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' = \'' + eDateEnd.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY 
        listQuery = []
        listQuery.append(query1)
        listQuery.append(query2)
        
        result = self.executeListOfQuery(listQuery)
   
        if result[1][0][0] == 0 or result[0][0][0] == 0 :
            self.lDataHealth = self.lDataHealth + "Energy from Panels ; "
        if result[1][0][0] == None :
            self.lTexte = "qry_EnergySolarPanels_From_To, date END"
        if result[0][0][0] == None :
            self.lTexte = "qry_EnergySolarPanels_From_To, date BEGIN"
        if self.lTexte == "Yes":
            return (((result[1][0][0] - result[0][0][0])/(0.988*4181))*(1.1015*3720))
        else:
            return 0
    
    def qry_EnergyCHPtoDHW(self, eDateBegin, eDateEnd):
        query1 = 'SELECT ' + self.lSensorCHPtoDHW + self.lSensor_Energy + ' FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' = \'' + eDateBegin.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY 
        query2 = 'SELECT ' + self.lSensorCHPtoDHW + self.lSensor_Energy + ' FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' = \'' + eDateEnd.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY 
        listQuery = []
        listQuery.append(query1)
        listQuery.append(query2)
        result = self.executeListOfQuery(listQuery)
        if result[1][0][0] == 0 or result[0][0][0] == 0 :
            self.lDataHealth = self.lDataHealth + "Energy from CHP to DHW ; "
        if result[1][0][0] == None :
            self.lTexte = "qry_EnergyCHPtoDHW, date END"
        if result[0][0][0] == None :
            self.lTexte = "qry_EnergyCHPtoDHW, date BEGIN"
        if self.lTexte == "Yes":
            return ((result[1][0][0] - result[0][0][0])/1000)
        else:
            return 0
    
    def qry_EnergyCHPtoSH(self, eDateBegin, eDateEnd):
        query1 = 'SELECT ' + self.lSensorCHPtoSH + self.lSensor_Energy + ' FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' = \'' + eDateBegin.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY  
        query2 = 'SELECT ' + self.lSensorCHPtoSH + self.lSensor_Energy + ' FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' = \'' + eDateEnd.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY  
        listQuery = []
        listQuery.append(query1)
        listQuery.append(query2)
        result = self.executeListOfQuery(listQuery)
        if result[1][0][0] == 0 or result[0][0][0] == 0 :
            self.lDataHealth = self.lDataHealth + "Energy from CHP to SH ; "
        if result[1][0][0] == None :
            self.lTexte = "qry_EnergyCHPtoSH, date END"
        if result[0][0][0] == None :
            self.lTexte = "qry_EnergyCHPtoSH, date BEGIN"
        if self.lTexte == "Yes":
            return (result[1][0][0] - result[0][0][0])
        else:
            return 0
    
    def qry_EnergyBoiler1(self, eDateBegin, eDateEnd):
        query1 = 'SELECT ' + self.lSensorBoiler1 + self.lSensor_Energy + ' FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' = \'' + eDateBegin.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY 
        query2 = 'SELECT ' + self.lSensorBoiler1 + self.lSensor_Energy + ' FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' = \'' + eDateEnd.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY   
        listQuery = []
        listQuery.append(query1)
        listQuery.append(query2)
        result = self.executeListOfQuery(listQuery)
        if result[1][0][0] == 0 or result[0][0][0] == 0 :
            self.lDataHealth = self.lDataHealth + "Energy of Boiler 1 ; "
        if result[1][0][0] == None :
            self.lTexte = "qry_EnergyBoiler1, date END"
        if result[0][0][0] == None :
            self.lTexte = "qry_EnergyBoiler1, date BEGIN"
        if self.lTexte == "Yes":
            return (result[1][0][0] - result[0][0][0])
        else:
            return 0
    
    def qry_EnergyBoiler2(self, eDateBegin, eDateEnd):
        query1 = 'SELECT ' + self.lSensorBoiler2 + self.lSensor_Energy + ' FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' = \'' + eDateBegin.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY  
        query2 = 'SELECT ' + self.lSensorBoiler2 + self.lSensor_Energy + ' FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' = \'' + eDateEnd.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY  
        listQuery = []
        listQuery.append(query1)
        listQuery.append(query2)
        result = self.executeListOfQuery(listQuery)
        if result[1][0][0] == 0 or result[0][0][0] == 0 :
            self.lDataHealth = self.lDataHealth + "Energy from Boiler 2 ; "
        if result[1][0][0] == None :
            self.lTexte = "qry_EnergyBoiler2, date END"
        if result[0][0][0] == None :
            self.lTexte = "qry_EnergyBoiler2, date BEGIN"
        if self.lTexte == "Yes":
            return ((result[1][0][0] - result[0][0][0])/1000)
        else:
            return 0
        
    def qry_EnergyBoilers(self, eDateBegin, eDateEnd):
        if eDateBegin < datetime.datetime(2015,2,6,0,0,0):
            query10 = 'SELECT ' + self.lSensorBoilerToSH + self.lSensor_Energy + ' FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' = \'' + eDateBegin.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY  
            query11 = 'SELECT ' + self.lSensorSHtoDHW + self.lSensor_Energy + ' FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' = \'' + eDateBegin.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY  
            query20 = 'SELECT ' + self.lSensorBoilerToSH + self.lSensor_Energy + ' FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' = \'' + datetime.datetime(2015,2,6,0,0,0).strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY  
            query21 = 'SELECT ' + self.lSensorSHtoDHW + self.lSensor_Energy + ' FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' = \'' + datetime.datetime(2015,2,6,0,0,0).strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY  
            listQuery = []
            listQuery.append(query10)
            listQuery.append(query11)
            listQuery.append(query20)
            listQuery.append(query21)
            result = self.executeListOfQuery(listQuery)
            if result[0][0][0] == None :
                self.lTexte = "qry_EnergyBoilers, date BEGIN"
            if result[1][0][0] == None :
                self.lTexte = "qry_EnergyBoilers, date BEGIN"
            if result[2][0][0] == None :
                self.lTexte = "qry_EnergyBoilers, date END"
            if result[3][0][0] == None :
                self.lTexte = "qry_EnergyBoilers, date END"
            if self.lTexte == "Yes":
                return (result[2][0][0] + (result[3][0][0]/1000) - (result[0][0][0] + (result[1][0][0]/1000))) + self.qry_EnergyBoiler1(datetime.datetime(2015,2,6,0,0,0), eDateEnd) + (self.qry_EnergyBoiler2(datetime.datetime(2015,2,6,0,0,0), eDateEnd))
            else:
                return 0
        else:
            return self.qry_EnergyBoiler1(eDateBegin, eDateEnd) + (self.qry_EnergyBoiler2(eDateBegin, eDateEnd))
            
    def qry_EnergyConsumptionForSH(self, eDateBegin, eDateEnd):
        if eDateBegin >= datetime.datetime(2015,4,10,0,0,0):
            query1 = 'SELECT ' + self.lSensorSHdemand + self.lSensor_Energy + ' FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' = \'' + eDateBegin.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY  
            query2 = 'SELECT ' + self.lSensorSHdemand + self.lSensor_Energy + ' FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' = \'' + eDateEnd.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY  
            listQuery = []
            listQuery.append(query1)
            listQuery.append(query2)
            result = self.executeListOfQuery(listQuery)
            if result[1][0][0] == 0 or result[0][0][0] == 0 :
                self.lDataHealth = self.lDataHealth + "Energy consumed for SH ; " 
            if result[1][0][0] == None :
                self.lTexte = "qry_EnergyConsumptionForSH1, date END"
            if result[0][0][0] == None :
                self.lTexte = "qry_EnergyConsumptionForSH2, date BEGIN"
            if self.lTexte == "Yes":
                return (result[1][0][0] - result[0][0][0])
            else:
                return 0
        else:
            query1 = 'SELECT ' + self.lSensorBoilerToSH + self.lSensor_Energy + ' + ' + self.lSensorCHPtoSH + self.lSensor_Energy + '  FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' = \'' + eDateBegin.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY   
            query2 = 'SELECT ' + self.lSensorBoilerToSH + self.lSensor_Energy + ' + ' + self.lSensorCHPtoSH + self.lSensor_Energy + '  FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' = \'' + datetime.datetime(2015,4,7,7,41,0).strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY  
            query3 = 'SELECT ' + self.lSensorSHdemand + self.lSensor_Energy + ' FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' = \'' + datetime.datetime(2015,4,8,7,41,0).strftime("%Y-%m-%d %H:%M:%S") + '\''
            query4 = 'SELECT ' + self.lSensorSHdemand + self.lSensor_Energy + ' FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' = \'' + eDateEnd.strftime("%Y-%m-%d %H:%M:%S") + '\'' 
            listQuery = []
            listQuery.append(query1)
            listQuery.append(query2)
            listQuery.append(query3)
            listQuery.append(query4)
            result = self.executeListOfQuery(listQuery)
            if result[3][0][0] == 0 or result[2][0][0] == 0 or result[1][0][0] == 0 or result[0][0][0] == 0 :
                self.lDataHealth = self.lDataHealth + "Energy consumed for SH ; " 
            if result[0][0][0] == None :
                self.lTexte = "qry_EnergyConsumptionForSH3, date BEGIN"
            if result[1][0][0] == None :
                self.lTexte = "qry_EnergyConsumptionForSH, sensor date"
            if result[2][0][0] == None :
                self.lTexte = "qry_EnergyConsumptionForSH, sensor date"
            if result[3][0][0] == None :
                self.lTexte = "qry_EnergyConsumptionForSH, date END" 
            if self.lTexte == "Yes":
                return (result[1][0][0] - result[0][0][0] + result[3][0][0] - result[2][0][0])
            else:
                return 0
            
    def qry_EnergyConsumptionForDHW(self, eDateBegin, eDateEnd):
        query1 = 'SELECT ' + self.lSensorDHWdemand + self.lSensor_Energy + ' FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' = \'' + eDateBegin.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY  
        query2 = 'SELECT ' + self.lSensorDHWdemand + self.lSensor_Energy + ' FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' = \'' + eDateEnd.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY
        listQuery = []
        listQuery.append(query1)
        listQuery.append(query2)
        result = self.executeListOfQuery(listQuery)
        if result[1][0][0] == 0 or result[0][0][0] == 0 :
            self.lDataHealth = self.lDataHealth + "Energy consumed for DHW ; "
        if result[1][0][0] == None :
            self.lTexte = "qry_EnergyConsumptionForDHW, date END"
        if result[0][0][0] == None :
            self.lTexte = "qry_EnergyConsumptionForDHW, date BEGIN"
        if self.lTexte == "Yes":
            return (result[1][0][0] - result[0][0][0])
        else:
            return 0
    
    def qry_EnergyProvidedToSHonlyByBoilers(self, eDateBegin, eDateEnd):
        query1 = 'SELECT ' + self.lSensorBoilerToSH + self.lSensor_Energy + ' FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' = \'' + eDateBegin.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY  
        query2 = 'SELECT ' + self.lSensorBoilerToSH + self.lSensor_Energy + ' FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' = \'' + eDateEnd.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY  
        listQuery = []
        listQuery.append(query1)
        listQuery.append(query2)
        result = self.executeListOfQuery(listQuery)
        if result[1][0][0] == 0 or result[0][0][0] == 0 :
            self.lDataHealth = self.lDataHealth + "Energy provided only by Boilers to SH ; "
        if result[1][0][0] == None :
            self.lTexte = "qry_EnergyProvidedToSHonlyByBoilers, date END"
        if result[0][0][0] == None :
            self.lTexte = "qry_EnergyProvidedToSHonlyByBoilers, date BEGIN"
        if self.lTexte == "Yes":
            return (result[1][0][0] - result[0][0][0])
        else:
            return 0
    
    def qry_EnergyTakenFromSHtoDHW(self, eDateBegin, eDateEnd):
        query1 = 'SELECT ' + self.lSensorSHtoDHW + self.lSensor_Energy + ' FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' = \'' + eDateBegin.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY   
        query2 = 'SELECT ' + self.lSensorSHtoDHW + self.lSensor_Energy + ' FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' = \'' + eDateEnd.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY   
        listQuery = []
        listQuery.append(query1)
        listQuery.append(query2)
        result = self.executeListOfQuery(listQuery)
        if result[1][0][0] == 0 or result[0][0][0] == 0 :
            self.lDataHealth = self.lDataHealth + "Energy transfered from SH to DHW ; "
        if result[1][0][0] == None :
            self.lTexte = "qry_EnergyTakenFromSHtoDHW, date END"
        if result[0][0][0] == None :
            self.lTexte = "qry_EnergyTakenFromSHtoDHW, date BEGIN"
        if self.lTexte == "Yes":
            return ((result[1][0][0] - result[0][0][0])/1000)
        else:
            return 0
    
    def qry_EnergyConsumedInDHWRecirculated(self, eDateBegin, eDateEnd):
        query1 = 'SELECT ' + self.lSensorRecirculation + self.lSensor_Energy + ' FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' = \'' + eDateBegin.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY 
        query2 = 'SELECT ' + self.lSensorRecirculation + self.lSensor_Energy + ' FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' = \'' + eDateEnd.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY  
        listQuery = []
        listQuery.append(query1)
        listQuery.append(query2)
        result = self.executeListOfQuery(listQuery)
        if result[1][0][0] == 0 or result[0][0][0] == 0 :
            self.lDataHealth = self.lDataHealth + "Energy consumed by the DHW recirculation ; "
        if result[1][0][0] == None :
            self.lTexte = "qry_EnergyConsumedInDHWRecirculated, date END"
        if result[0][0][0] == None :
            self.lTexte = "qry_EnergyConsumedInDHWRecirculated, date BEGIN"
        if self.lTexte == "Yes":
            return ((result[1][0][0] - result[0][0][0])/1000)
        else:
            return 0
    
    def qry_EnergySolarPanelNET(self, eDateBegin, eDateEnd):
        query1 = 'SELECT ' + self.lSensorSolarNet + self.lSensor_Energy + ' FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' = \'' + eDateBegin.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY  
        query2 = 'SELECT ' + self.lSensorSolarNet + self.lSensor_Energy + ' FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' = \'' + eDateEnd.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY  
        listQuery = []
        listQuery.append(query1)
        listQuery.append(query2)
        result = self.executeListOfQuery(listQuery)
        if result[1][0][0] == 0 or result[0][0][0] == 0 :
            self.lDataHealth = self.lDataHealth + "Energy from solar panel, NET ; " 
        if result[1][0][0] == None :
            self.lTexte = "qry_EnergySolarPanelNET, date END"
        if result[0][0][0] == None :
            self.lTexte = "qry_EnergySolarPanelNET, date BEGIN"
        if self.lTexte == "Yes":
            return (result[1][0][0] - result[0][0][0])
        else:
            return 0
    
    def qry_GetGasConsumption_Boil1(self, eDateBegin, eDateEnd):
        query1 = 'SELECT ' + self.lSensorGasBoil1 + ' FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' = \'' + eDateBegin.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY  
        query2 = 'SELECT ' + self.lSensorGasBoil1 + ' FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' = \'' + eDateEnd.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY  
        listQuery = []
        listQuery.append(query1)
        listQuery.append(query2)
        result = self.executeListOfQuery(listQuery)
        if result[1][0][0] == 0 or result[0][0][0] == 0 :
            self.lDataHealth = self.lDataHealth + "Gas consumption of Boiler 1 ; "
        if result[1][0][0] == None :
            self.lTexte = "qry_GetGasConsumption_Boil1, date END"
        if result[0][0][0] == None :
            self.lTexte = "qry_GetGasConsumption_Boil1, date BEGIN"
        if self.lTexte == "Yes":
            return (result[1][0][0] - result[0][0][0])
        else:
            return 0
        
    def qry_GetGasConsumption_Boil2(self, eDateBegin, eDateEnd):
        query1 = 'SELECT ' + self.lSensorGasBoil2 + ' FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' = \'' + eDateBegin.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY  
        query2 = 'SELECT ' + self.lSensorGasBoil2 + ' FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' = \'' + eDateEnd.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY   
        listQuery = []
        listQuery.append(query1)
        listQuery.append(query2)
        result = self.executeListOfQuery(listQuery)
        if result[1][0][0] == 0 or result[0][0][0] == 0 :
            self.lDataHealth = self.lDataHealth + "Gas consumption of Boiler 2 ; "
        if result[1][0][0] == None :
            self.lTexte = "qry_GetGasConsumption_Boil2, date END"
        if result[0][0][0] == None :
            self.lTexte = "qry_GetGasConsumption_Boil2, date BEGIN"
        if self.lTexte == "Yes":
            return (result[1][0][0] - result[0][0][0])
        else:
            return 0
        
    def qry_GetGasConsumption_CHP(self, eDateBegin, eDateEnd):
        query1 = 'SELECT ' + self.lSensorGasCHP + ' FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' = \'' + eDateBegin.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY  
        query2 = 'SELECT ' + self.lSensorGasCHP + ' FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' = \'' + eDateEnd.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY  
        listQuery = []
        listQuery.append(query1)
        listQuery.append(query2)
        result = self.executeListOfQuery(listQuery)
        if result[1][0][0] == 0 or result[0][0][0] == 0 :
            self.lDataHealth = self.lDataHealth + "Gas consumption of CHP ; "
        if result[1][0][0] == None :
            self.lTexte = "qry_GetGasConsumption_CHP, date END"
        if result[0][0][0] == None :
            self.lTexte = "qry_GetGasConsumption_CHP, date BEGIN"
        if self.lTexte == "Yes":
            return (result[1][0][0] - result[0][0][0])
        else:
            return 0
    
    def qry_GetElecExported(self, eDateBegin, eDateEnd):
        query1 = 'SELECT ' + self.lSensorElecExported + self.lSensor_Energy + ' FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' = \'' + eDateBegin.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY  
        query2 = 'SELECT ' + self.lSensorElecExported + self.lSensor_Energy + ' FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' = \'' + eDateEnd.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY  
        listQuery = []
        listQuery.append(query1)
        listQuery.append(query2)
        result = self.executeListOfQuery(listQuery)
        if result[1][0][0] == 0 or result[0][0][0] == 0 :
            self.lDataHealth = self.lDataHealth + "Electricity exported from CHP ; "
        if result[1][0][0] == None :
            self.lTexte = "qry_GetElecExportedByCHP, date END"
        if result[0][0][0] == None :
            self.lTexte = "qry_GetElecExportedByCHP, date BEGIN"
        if self.lTexte == "Yes":
            return (result[1][0][0] - result[0][0][0])
        else:
            return 0

    def qry_GetElecConsumedByCHP(self, eDateBegin, eDateEnd):
        query1 = 'SELECT ' + self.lSensorCHPconsumption + self.lSensor_Energy + ' FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' = \'' + eDateBegin.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY  
        query2 = 'SELECT ' + self.lSensorCHPconsumption + self.lSensor_Energy + ' FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' = \'' + eDateEnd.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY  
        listQuery = []
        listQuery.append(query1)
        listQuery.append(query2)
        result = self.executeListOfQuery(listQuery)
        if result[1][0][0] == 0 or result[0][0][0] == 0 :
            self.lDataHealth = self.lDataHealth + "Electricity consumed by CHP ; "
        if result[1][0][0] == None :
            self.lTexte = "qry_GetElecConsumedByCHP, date END"
        if result[0][0][0] == None :
            self.lTexte = "qry_GetElecConsumedByCHP, date BEGIN"
        if self.lTexte == "Yes":
            return (result[1][0][0] - result[0][0][0])
        else:
            return 0

    def qry_GetElecProducedByCHP(self, eDateBegin, eDateEnd):
        query1 = 'SELECT ' + self.lSensorCHPnetProd + self.lSensor_Energy + ' FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' = \'' + eDateBegin.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY  
        query2 = 'SELECT ' + self.lSensorCHPnetProd + self.lSensor_Energy + ' FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' = \'' + eDateEnd.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY  
        listQuery = []
        listQuery.append(query1)
        listQuery.append(query2)
        result = self.executeListOfQuery(listQuery)
        if result[1][0][0] == 0 or result[0][0][0] == 0 :
            self.lDataHealth = self.lDataHealth + "Electricity produced by CHP ; "
        if result[1][0][0] == None :
            self.lTexte = "qry_GetElecProducedByCHP, date END"
        if result[0][0][0] == None :
            self.lTexte = "qry_GetElecProducedByCHP, date BEGIN"
        if self.lTexte == "Yes":
            return (result[1][0][0] - result[0][0][0])
        else:
            return 0

    def qry_GetValueHeatDemand_Tab(self, eDateBegin, eDateEnd):
        #query1 = 'SELECT PlantDataDB.dbo.dp11.pointInTime, PlantDataDB.dbo.dp11.dk561_4, PlantDataDB.dbo.dp11.dk570_4  FROM PlantDataDB.dbo.dp11, PlantDataDB.dbo.dp12 WHERE PlantDataDB.dbo.dp11.pointInTime > \'' + eDateBegin.strftime("%Y-%m-%d %H:%M:%S") + '\' AND PlantDataDB.dbo.dp11.pointInTime < \'' + eDateEnd.strftime("%Y-%m-%d %H:%M:%S") + '\' AND PlantDataDB.dbo.dp11.pointInTime = PlantDataDB.dbo.dp12.pointInTime ORDER BY PlantDataDB.dbo.dp11.pointInTime DESC'
        query1 = 'SELECT ' + self.ptInTime + ', ' + self.lSensorSHdemand + self.lSensor_Power + ', ' + self.lSensorDHWdemand + self.lSensor_Power + ' FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' > \'' + eDateBegin.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.ptInTime + ' < \'' + eDateEnd.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY  
        return self.executeQuery(query1)
    
    def qry_GetValueElec_Tab(self, eDateBegin, eDateEnd):
        #query1 = 'SELECT PlantDataDB.dbo.dp11.pointInTime, PlantDataDB.dbo.dp12.dk100_4, PlantDataDB.dbo.dp12.dk110_4, PlantDataDB.dbo.dp11.dk101_4  FROM PlantDataDB.dbo.dp11, PlantDataDB.dbo.dp12 WHERE PlantDataDB.dbo.dp11.pointInTime > \'' + eDateBegin.strftime("%Y-%m-%d %H:%M:%S") + '\' AND PlantDataDB.dbo.dp11.pointInTime < \'' + eDateEnd.strftime("%Y-%m-%d %H:%M:%S") + '\' AND PlantDataDB.dbo.dp11.pointInTime = PlantDataDB.dbo.dp12.pointInTime ORDER BY PlantDataDB.dbo.dp11.pointInTime DESC'
        query1 = 'SELECT ' + self.ptInTime + ', ' + self.lSensorCHPconsumption + self.lSensor_Power + ', ' + self.lSensorCHPnetProd + self.lSensor_Power + ', ' + self.lSensorElecExported + self.lSensor_Power + ' FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' > \'' + eDateBegin.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.ptInTime + ' < \'' + eDateEnd.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY  
        return self.executeQuery(query1)

    def qry_GetSolarProductionGross_Tab(self, eDateBegin, eDateEnd):
        #query1 = 'SELECT pointInTime, dk510_4 FROM PlantDataDB.dbo.dp12 WHERE pointInTime > \'' + eDateBegin.strftime("%Y-%m-%d %H:%M:%S") + '\' AND pointInTime < \'' + eDateEnd.strftime("%Y-%m-%d %H:%M:%S") + '\''
        query1 = 'SELECT ' + self.ptInTime + ', ' + self.lSensorSolarGross + self.lSensor_Power + ' FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' > \'' + eDateBegin.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.ptInTime + ' < \'' + eDateEnd.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY  
        return self.executeQuery(query1)
    
    def qry_GetCHP_DeltaT_Tab(self, eDateBegin, eDateEnd):
        shSensor = self.lSensorSHdemand + self.lSensor_Power
        CHPflow = self.lSensorCHPtoSH + self.lSensor_TempFlow
        CHPreturn = self.lSensorCHPtoSH + self.lSensor_TempReturn
        query1 = 'SELECT ' + self.ptInTime + ', CASE WHEN ' + shSensor + ' > 0 THEN ' + CHPflow + ' - ' + CHPreturn + '  ELSE -999 END FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' > \'' + eDateBegin.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.ptInTime + ' < \'' + eDateEnd.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY  
        return self.executeQuery(query1)
    
    def qry_GetDataForSigmoidSH(self, eDateBegin, eDateEnd):
        query1 = 'SELECT ' + self.ptInTime + ', ' + self.lSensorSHdemand + self.lSensor_Power + ', ' + self.lSensorExtTemp + ' FROM ' + self.FROM + ' WHERE ' + self.ptInTime + ' > \'' + eDateBegin.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.ptInTime + ' < \'' + eDateEnd.strftime("%Y-%m-%d %H:%M:%S") + '\' AND ' + self.WHEREand + ' ORDER BY ' + self.ORDERBY  
        return self.executeQuery(query1)
    


    def plotPie_GrossProduction(self, eDateBegin, eDateEnd):
        self.resetStringAttribute()
        
        #dedicated production
        prod_CHPtoDHW = self.qry_EnergyCHPtoDHW(eDateBegin, eDateEnd)
        prod_CHPtoSH = self.qry_EnergyCHPtoSH(eDateBegin, eDateEnd)
        # Gross production
        production_Solar_Gross = self.qry_EnergySolarPanels(eDateBegin, eDateEnd)
        production_Boilers_Gross = self.qry_EnergyBoilers(eDateBegin, eDateEnd)
        production_CHP_Gross = prod_CHPtoDHW + prod_CHPtoSH
        
        if self.lTexte == "Yes" :
            self.beCarefulOfDataHealth()
            serie = pandas.Series([production_Solar_Gross, production_CHP_Gross, production_Boilers_Gross], name="GROSS PRODUCTION" )
            serie.plot(kind="pie" ,autopct='%.1f', figsize=(20,20), labels=['Solar production (' + str(round(production_Solar_Gross/1000,1)) + ' MWh)', 'CHP production (' + str(round(production_CHP_Gross/1000,1)) + ' MWh)', 'Boiler production (' + str(round(production_Boilers_Gross/1000,1)) + ' MWh)'])
            if self.autosave:
                matplotlib.pyplot.savefig(self.savePath + "chart_Pie - Gross Production")
                matplotlib.pyplot.close()
            else:
                matplotlib.pyplot.show()
        else:  
            print("")
            print("Data are not good at this date, choose another date.")
            print("can not draw 'GROSS PRODUCTION' ")
            print("Error : " + self.lTexte)
 
    def plotPie_HeatDemand(self, eDateBegin, eDateEnd):
        self.resetStringAttribute()
        
        #consumption
        recirculation_DHW= round(self.qry_EnergyConsumedInDHWRecirculated(eDateBegin, eDateEnd),1)
        demand_SH = self.qry_EnergyConsumptionForSH(eDateBegin, eDateEnd)
        demand_DHW = self.qry_EnergyConsumptionForDHW(eDateBegin, eDateEnd)
        
        if self.lTexte == "Yes" :
            self.beCarefulOfDataHealth()
            serie = pandas.Series([demand_SH, demand_DHW, recirculation_DHW], name="HEAT DEMAND")
            serie.plot(kind="pie", autopct='%.1f', figsize=(20,20), labels=['Space heating demand (' + str(round(demand_SH/1000,1)) + ' MWh)', 'Domestic hot water demand (' + str(round(demand_DHW/1000,1)) + ' MWh)', 'Recirculation in DHW (' + str(round(recirculation_DHW/1000,1)) + ' MWh)'])
            if self.autosave:
                matplotlib.pyplot.savefig(self.savePath + "chart_pie - Heat demand")
                matplotlib.pyplot.close()
            else:
                matplotlib.pyplot.show()
        else:  
            print("")
            print("Data are not good at this date, choose another date.")
            print("can not draw 'HEAT DEMAND' ")
            print("Error : " + self.lTexte)
   
    def plotPie_HeatForDHW(self, eDateBegin, eDateEnd):
        self.resetStringAttribute()
        
        #dedicated production
        prod_CHPtoDHW = self.qry_EnergyCHPtoDHW(eDateBegin, eDateEnd)
        # intern movement
        transfert_SHtoDHW = self.qry_EnergyTakenFromSHtoDHW(eDateBegin, eDateEnd)
        # net production
        prod_Solar_Net = self.qry_EnergySolarPanelNET(eDateBegin, eDateEnd)

        if self.lTexte == "Yes":
            self.beCarefulOfDataHealth()
            serie = pandas.Series([prod_CHPtoDHW, transfert_SHtoDHW, prod_Solar_Net], name='HEAT PRODUCTION FOR DHW')
            serie.plot(kind='pie', autopct='%.1f', figsize=(20,20), labels=['From CHP (' + str(round(prod_CHPtoDHW/1000,1)) + ' MWh)', 'From SH system (' + str(round(transfert_SHtoDHW/1000,1)) + ' MWh)', 'From Solar (' + str(round(prod_Solar_Net/1000,1)) + ' MWh)'])
            if self.autosave:
                matplotlib.pyplot.savefig(self.savePath + "chart_pie - Heat for DHW")
                matplotlib.pyplot.close()
            else:
                matplotlib.pyplot.show()
        else:
            print("")
            print("Data are not good at this date, choose another date.")
            print("can not draw 'HEAT PRODUCTION FOR DHW' ")
            print("Error : " + self.lTexte)
                 
    def plotPie_HeatForSH(self, eDateBegin, eDateEnd):
        self.resetStringAttribute()
        
        #dedicated production
        prod_CHPtoSH = self.qry_EnergyCHPtoSH(eDateBegin, eDateEnd)
        prod_BoilersToSH = self.qry_EnergyProvidedToSHonlyByBoilers(eDateBegin, eDateEnd)
        
        if self.lTexte == 'Yes':
            self.beCarefulOfDataHealth()
            serie = pandas.Series([prod_CHPtoSH, prod_BoilersToSH], name='HEAT PRODUCTION FOR SH')
            serie.plot(kind='pie', autopct='%.1f', figsize=(20,20), labels=['From CHP (' + str(round(prod_CHPtoSH/1000,1)) + ' MWh)', 'From Boilers (both) (' + str(round(prod_BoilersToSH/1000,1)) + ' MWh)'])
            if self.autosave:
                matplotlib.pyplot.savefig(self.savePath + "chart_pie - Heat for SH")
                matplotlib.pyplot.close()
            else:
                matplotlib.pyplot.show()
        else:
            print("")
            print("Data are not good at this date, choose another date.")
            print("can not draw 'HEAT PRODUCTION FOR SH' ")
            print("Error : " + self.lTexte)
        
    def plotPie_Losses(self, eDateBegin, eDateEnd):
        self.resetStringAttribute()
        
        # Gross production
        production_Solar_Gross = self.qry_EnergySolarPanels(eDateBegin, eDateEnd)
        production_Boilers = round(self.qry_EnergyBoilers(eDateBegin, eDateEnd),1)
        prod_CHPtoSH = round(self.qry_EnergyCHPtoSH(eDateBegin, eDateEnd),1)
        prod_CHPtoDHW = round(self.qry_EnergyCHPtoDHW(eDateBegin, eDateEnd),1)
        production_CHP = prod_CHPtoSH + prod_CHPtoDHW
        production_total = production_Boilers + production_CHP + production_Solar_Gross
        # net production
        prod_Solar_Net = self.qry_EnergySolarPanelNET(eDateBegin, eDateEnd)
        # intern movement
        transfert_SHtoDHW = self.qry_EnergyTakenFromSHtoDHW(eDateBegin, eDateEnd)
        #dedicated production
        prod_CHPtoDHW = self.qry_EnergyCHPtoDHW(eDateBegin, eDateEnd)
        prod_CHPtoSH = self.qry_EnergyCHPtoSH(eDateBegin, eDateEnd)
        prod_BoilersToSH = self.qry_EnergyProvidedToSHonlyByBoilers(eDateBegin, eDateEnd)
        #perceived energy
        perceived_DHW = prod_Solar_Net + transfert_SHtoDHW + prod_CHPtoDHW
        #consumption
        demand_SH = self.qry_EnergyConsumptionForSH(eDateBegin, eDateEnd)
        demand_DHW = self.qry_EnergyConsumptionForDHW(eDateBegin, eDateEnd)
        recirculation_DHW = self.qry_EnergyConsumedInDHWRecirculated(eDateBegin, eDateEnd)
        #losses
        losses_DWH_tank = perceived_DHW - demand_DHW - recirculation_DHW
        losses_Solar_tank = production_Solar_Gross - prod_Solar_Net
        losses_CHP_tank = prod_CHPtoSH + prod_BoilersToSH - demand_SH
        losses_Others = production_total - demand_SH - demand_DHW - recirculation_DHW - losses_CHP_tank - losses_DWH_tank - losses_Solar_tank
        #
        
        if self.lTexte == "Yes" :
            self.beCarefulOfDataHealth()
            serie = pandas.Series([losses_Others, losses_DWH_tank, losses_Solar_tank, losses_CHP_tank], name='LOSSES')
            serie = serie[serie>0]
            serie.plot(kind="pie", autopct='%.1f', labels=['Other losses (in pipe for example) (' + str(round(losses_Others/1000,1)) + ' MWh)', 'DHW tank (' + str(round(losses_DWH_tank/1000,1)) + ' MWh)', 'Solar tank (' + str(round(losses_Solar_tank/1000,1)) + ' MWh)', 'CHP tank (' + str(round(losses_CHP_tank/1000,1)) + ' MWh)'], figsize=(20,20))
            if self.autosave:
                matplotlib.pyplot.savefig(self.savePath + "chart_pie - Losses")
                matplotlib.pyplot.close()
            else:
                matplotlib.pyplot.show()
        else:  
            print("")
            print("Data are not good at this date, choose another date.")
            print("can not draw 'LOSSES' ")
            print("Error : " + self.lTexte)
        
    def plotPie_EnergyRepartition(self, eDateBegin, eDateEnd):
        self.resetStringAttribute()
        
        #dedicated production
        prod_CHPtoDHW = self.qry_EnergyCHPtoDHW(eDateBegin, eDateEnd)
        prod_CHPtoSH = self.qry_EnergyCHPtoSH(eDateBegin, eDateEnd)
        # Gross production
        production_Solar_Gross = self.qry_EnergySolarPanels(eDateBegin, eDateEnd)
        production_Boilers_Gross = self.qry_EnergyBoilers(eDateBegin, eDateEnd)
        production_CHP_Gross = prod_CHPtoDHW + prod_CHPtoSH
        #consumption
        demand_SH = self.qry_EnergyConsumptionForSH(eDateBegin, eDateEnd)
        demand_DHW = self.qry_EnergyConsumptionForDHW(eDateBegin, eDateEnd)
        DWH_recirculation = self.qry_EnergyConsumedInDHWRecirculated(eDateBegin, eDateEnd)
        #total
        total_EnergyProduced = production_Solar_Gross + production_CHP_Gross + production_Boilers_Gross
        total_EnergyDemand = demand_DHW + demand_SH
        total_Losses = total_EnergyProduced - total_EnergyDemand - DWH_recirculation
        
        if self.lTexte == "Yes" :
            self.beCarefulOfDataHealth()
            serie = pandas.Series([total_EnergyDemand, total_Losses, DWH_recirculation], name='ENERGY REPARTITION')
            serie.plot(kind="pie", autopct='%.1f', figsize=(20,20), labels=['Heat demand from SH and DHW(' + str(round(total_EnergyDemand/1000,1)) + ' MWh)','Heat lost (' + str(round(total_Losses/1000,1)) + ' MWh)','Lost in recirculation (' + str(round(DWH_recirculation/1000,1)) + ' MWh)'])
            if self.autosave:
                matplotlib.pyplot.savefig(self.savePath + "chart_pie - Energy repartition")
                matplotlib.pyplot.close()
            else:
                matplotlib.pyplot.show()
        else:  
            print("")
            print("Data are not good at this date, choose another date.")
            print("can not draw 'ENERGY REPARTITION' ")
            print("Error : " + self.lTexte)
        
    def plotPie_AllEnergy(self, eDateBegin, eDateEnd):
        self.resetStringAttribute()
        
        #dedicated production
        prod_BoilersToSH = self.qry_EnergyProvidedToSHonlyByBoilers(eDateBegin, eDateEnd)
        prod_CHPtoDHW = self.qry_EnergyCHPtoDHW(eDateBegin, eDateEnd)
        prod_CHPtoSH = self.qry_EnergyCHPtoSH(eDateBegin, eDateEnd)
        # net production
        prod_Solar_Net = self.qry_EnergySolarPanelNET(eDateBegin, eDateEnd)
        # Gross production
        production_Solar_Gross = self.qry_EnergySolarPanels(eDateBegin, eDateEnd)
        production_Boilers_Gross = self.qry_EnergyBoilers(eDateBegin, eDateEnd)
        production_CHP_Gross = prod_CHPtoDHW + prod_CHPtoSH
        #consumption
        demand_SH = self.qry_EnergyConsumptionForSH(eDateBegin, eDateEnd)
        demand_DHW = self.qry_EnergyConsumptionForDHW(eDateBegin, eDateEnd)
        # intern movement
        transfert_SHtoDHW = self.qry_EnergyTakenFromSHtoDHW(eDateBegin, eDateEnd)
        #perceived energy
        perceived_DHW = prod_Solar_Net + transfert_SHtoDHW + prod_CHPtoDHW
        # losses
        losses_DWH_recirculation = self.qry_EnergyConsumedInDHWRecirculated(eDateBegin, eDateEnd)
        losses_DWH_tank = perceived_DHW - demand_DHW - losses_DWH_recirculation
        losses_Solar_tank = production_Solar_Gross - prod_Solar_Net
        losses_CHP_tank = prod_CHPtoSH + prod_BoilersToSH - demand_SH
        losses_other = production_Boilers_Gross + production_CHP_Gross + production_Solar_Gross - demand_DHW - demand_SH - losses_DWH_recirculation - losses_DWH_tank - losses_Solar_tank - losses_CHP_tank
        
        if self.lTexte == "Yes" :
            self.beCarefulOfDataHealth()
            serie = pandas.Series([production_CHP_Gross, production_Boilers_Gross, production_Solar_Gross, demand_SH, demand_DHW, losses_Solar_tank, losses_DWH_tank, losses_CHP_tank, losses_DWH_recirculation, losses_other])#, name='ENERGY FROM ' + eDateBegin.strftime("%Y-%m-%d %H:%M:%S") + ' to ' + eDateEnd.strftime("%Y-%m-%d %H:%M:%S"))
            serie = serie[serie>0]
            serie.plot(kind='pie', autopct='%.1f', figsize=(20,20), colors=('g', 'g', 'g', 'b', 'b', 'r', 'r', 'r', 'r', 'r'), labels=['Prod_CHP (' + str(round(production_CHP_Gross/1000,1)) + ' MWh)', 'Prod_Boiler (' + str(round(production_Boilers_Gross/1000,1)) + ' MWh)', 'Prod_Solar (' + str(round(production_Solar_Gross/1000,1)) + ' MWh)', 'Demand_SH (' + str(round(demand_SH/1000,1)) + ' MWh)', 'Demand_DHW (' + str(round(demand_DHW/1000,1)) + ' MWh)', 'Losses_Solar (' + str(round(losses_Solar_tank/1000,1)) + ' MWh)', 'Losses_DHW (' + str(round(losses_DWH_tank/1000,1)) + ' MWh)', 'Losses_CHP (' + str(round(losses_CHP_tank/1000,1)) + ' MWh)', 'Losses_Recirculation (' + str(round(losses_DWH_recirculation/1000,1)) + ' MWh)', "Losses_Other (" + str(round(losses_other/1000,1))+ ' MWh)'])
            if self.autosave:
                matplotlib.pyplot.savefig(self.savePath + "chart_pie - All energy")
                matplotlib.pyplot.close()
            else:
                matplotlib.pyplot.show()
        else:  
            print("")
            print("Data are not good at this date, choose another date.")
            print("can not draw 'ALL ENERGY' ")
            print("Error : " + self.lTexte)
            
    def plotLineChart_HeatDemand_AvgDays(self, eDateBegin, eDateEnd):
        self.resetStringAttribute()
       
        tab_Data = self.qry_GetValueHeatDemand_Tab(eDateBegin, eDateEnd)
        nbDay = self.utilities_GetNumberOfDays(eDateBegin, eDateEnd)
        tab_Final_Date = []
        tab_Final_SH = []
        tab_Final_DHW = []
        tab_Cpt_SH = []
        tab_Cpt_DHW = []
        
        for i in range(0,nbDay):
            tab_Final_SH.append(0)
            tab_Final_DHW.append(0)
            tab_Cpt_SH.append(0)
            tab_Cpt_DHW.append(0)
        
        for i in range(0,nbDay):
            tab_Final_Date.append(eDateBegin + datetime.timedelta(i))
        
        for i in range(0,len(tab_Data[0])):
            dayNumber = self.utilities_GetNumberOfDays(eDateBegin, tab_Data[0][i][0])
            if tab_Data[0][i][1] != None:
                tab_Final_SH[dayNumber] = tab_Final_SH[dayNumber] + tab_Data[0][i][1]
                tab_Cpt_SH[dayNumber] = tab_Cpt_SH[dayNumber] + 1
            if tab_Data[0][i][2] != None:
                tab_Final_DHW[dayNumber] = tab_Final_DHW[dayNumber] + tab_Data[0][i][2]
                tab_Cpt_DHW[dayNumber] = tab_Cpt_DHW[dayNumber] + 1
                
        for i in range(0,nbDay):
            if tab_Cpt_SH[i] != 0:
                tab_Final_SH[i] = (tab_Final_SH[i]/tab_Cpt_SH[i])*24/1000
            else:
                tab_Final_SH[i] = 0
            if tab_Cpt_DHW[i] != 0:
                tab_Final_DHW[i] = (tab_Final_DHW[i]/tab_Cpt_DHW[i])*24/1000
            else:
                tab_Cpt_DHW[i] = 0 

        graph = pandas.DataFrame(tab_Final_DHW, columns=['DHW heat demand (kWh/day) - Avg on days'])#, name="DHW demand, average on days")
        graph['Date'] = pandas.Series(tab_Final_Date)
        graph.plot(x='Date', y='DHW heat demand (kWh/day) - Avg on days')
        if self.autosave:
            matplotlib.pyplot.savefig(self.savePath + "chart_line - DHW Heat demand (kWh over day) - Avg on days")
        else:
            matplotlib.pyplot.show()
        
        graph2 = pandas.DataFrame(tab_Final_SH, columns=["SH heat demand (kWh/day) - Avg on days"])#, name="DHW demand, average on days")
        graph2['Date'] = pandas.Series(tab_Final_Date)
        graph2.plot(x='Date', y='SH heat demand (kWh/day) - Avg on days')
        if self.autosave:
            matplotlib.pyplot.savefig(self.savePath + "chart_line - SH Heat demand (kWh over day) - Avg on days")
            matplotlib.pyplot.close()
        else:
            matplotlib.pyplot.show()
        
    def plotLineChart_HeatDemand_AvgOnWeekDay(self, eDateBegin, eDateEnd):
        self.resetStringAttribute()
        
        tab_Data = self.qry_GetValueHeatDemand_Tab(eDateBegin, eDateEnd)
        
        tab_Final_Date = []
        tab_Final_SH = []
        tab_Final_DHW = []
        tab_Cpt_SH = []
        tab_Cpt_DHW = []
        
        tab_Final_Date.append("Monday")
        tab_Final_Date.append("Tuesday")
        tab_Final_Date.append("Wednesday")
        tab_Final_Date.append("Thursday")
        tab_Final_Date.append("Friday")
        tab_Final_Date.append("Saturday")
        tab_Final_Date.append("Sunday")
        for i in range(0,7):
            tab_Final_SH.append(0)
            tab_Final_DHW.append(0)
            tab_Cpt_DHW.append(0)
            tab_Cpt_SH.append(0)
        
        for i in range(0,len(tab_Data[0])):
            dayOfWeek = self.utilities_GetDayOfWeek(tab_Data[0][i][0])
            if tab_Data[0][i][1] != None:
                tab_Final_SH[dayOfWeek] = tab_Final_SH[dayOfWeek] + tab_Data[0][i][1]
                tab_Cpt_SH[dayOfWeek] = tab_Cpt_SH[dayOfWeek] + 1
            if tab_Data[0][i][2] != None:
                tab_Final_DHW[dayOfWeek] = tab_Final_DHW[dayOfWeek] + tab_Data[0][i][2]
                tab_Cpt_DHW[dayOfWeek] = tab_Cpt_DHW[dayOfWeek] + 1 
                
        for i in range(0,7):
            tab_Final_SH[i] = (tab_Final_SH[i] / tab_Cpt_SH[i])*24/1000
            tab_Final_DHW[i] = (tab_Final_DHW[i] / tab_Cpt_DHW[i])*24/1000
            
        graph = pandas.DataFrame(tab_Final_DHW, columns=['DHW heat demand (kWh/day) - Avg on Weekdays'])#, name="DHW demand, average on days")
        graph['Date'] = pandas.Series(tab_Final_Date)
        graph.plot(x='Date', y='DHW heat demand (kWh/day) - Avg on Weekdays')
        if self.autosave:
            matplotlib.pyplot.savefig(self.savePath + "chart_line - DHW Heat demand (kWh over day) - Avg on Weekdays")
        else:
            matplotlib.pyplot.show()
        
        graph2 = pandas.DataFrame(tab_Final_SH, columns=["SH heat demand (kWh/day) - Avg on Weekdays"])#, name="DHW demand, average on days")
        graph2['Date'] = pandas.Series(tab_Final_Date)
        graph2.plot(x='Date', y='SH heat demand (kWh/day) - Avg on Weekdays')
        if self.autosave:
            matplotlib.pyplot.savefig(self.savePath + "chart_line - SH Heat demand (kWh over day) - Avg on Weekdays")
            matplotlib.pyplot.close()
        else:
            matplotlib.pyplot.show()

    def plotLineChart_HeatDemand_AvgOnMonth(self, eDateBegin, eDateEnd):
        self.resetStringAttribute()
        
        tab_Data = self.qry_GetValueHeatDemand_Tab(eDateBegin, eDateEnd)
        
        tab_Final_Date = []
        tab_Final_SH = []
        tab_Final_DHW = []
        tab_Cpt_SH = []
        tab_Cpt_DHW = []
        
        tab_Final_Date.append("January")
        tab_Final_Date.append("February")
        tab_Final_Date.append("March")
        tab_Final_Date.append("April")
        tab_Final_Date.append("May")
        tab_Final_Date.append("June")
        tab_Final_Date.append("July")
        tab_Final_Date.append("August")
        tab_Final_Date.append("September")
        tab_Final_Date.append("October")
        tab_Final_Date.append("November")
        tab_Final_Date.append("December")
        for i in range(0,12):
            tab_Final_SH.append(0)
            tab_Final_DHW.append(0)
            tab_Cpt_DHW.append(0)
            tab_Cpt_SH.append(0)
        
        for i in range(0,len(tab_Data[0])):
            monthNumber = self.utilities_GetMonth(tab_Data[0][i][0])
            if tab_Data[0][i][1] != None:
                tab_Final_SH[monthNumber] = tab_Final_SH[monthNumber] + tab_Data[0][i][1]
                tab_Cpt_SH[monthNumber] = tab_Cpt_SH[monthNumber] + 1
            if tab_Data[0][i][2] != None:
                tab_Final_DHW[monthNumber] = tab_Final_DHW[monthNumber] + tab_Data[0][i][2]
                tab_Cpt_DHW[monthNumber] = tab_Cpt_DHW[monthNumber] + 1 
                
        for i in range(0,12):
            if tab_Cpt_SH[i] != 0:
                tab_Final_SH[i] = (tab_Final_SH[i] / (tab_Cpt_SH[i]))*24*7/1000
            else:
                tab_Final_SH[i] = 0
            if tab_Cpt_DHW[i] != 0:
                tab_Final_DHW[i] = (tab_Final_DHW[i] / tab_Cpt_DHW[i])*24*7/1000
            else:
                tab_Final_DHW[i] = 0
            
        graph = pandas.DataFrame(tab_Final_DHW, columns=['DHW heat demand (kWh/week) - Avg on Month'])#, name="DHW demand, average on days")
        graph['Date'] = pandas.Series(tab_Final_Date)
        graph.plot(x='Date', y='DHW heat demand (kWh/week) - Avg on Month')
        if self.autosave:
            matplotlib.pyplot.savefig(self.savePath + "chart_line - DHW Heat demand (kWh over week) - Avg on Month")
        else:
            matplotlib.pyplot.show()
        
        graph2 = pandas.DataFrame(tab_Final_SH, columns=["SH heat demand (kWh/week) - Avg on Month"])#, name="DHW demand, average on days")
        graph2['Date'] = pandas.Series(tab_Final_Date)
        graph2.plot(x='Date', y='SH heat demand (kWh/week) - Avg on Month')
        if self.autosave:
            matplotlib.pyplot.savefig(self.savePath + "chart_line - SH Heat demand (kWh over week) - Avg on Month")
            matplotlib.pyplot.close()
        else:
            matplotlib.pyplot.show()
    
    def plotLineChart_HeatDemand_AvgOnOneDay(self, eDateBegin, eDateEnd):
        self.resetStringAttribute()
        
        tab_Data = self.qry_GetValueHeatDemand_Tab(eDateBegin, eDateEnd)
        
        tab_Final_Date = []
        tab_Final_SH = []
        tab_Final_DHW = []
        tab_Cpt_SH = []
        tab_Cpt_DHW = []
        
        for i in range(0,1440):
            tab_Final_Date.append(datetime.time(i//60, i % 60))
            tab_Final_SH.append(0)
            tab_Final_DHW.append(0)
            tab_Cpt_DHW.append(0)
            tab_Cpt_SH.append(0)
        
        for i in range(0,len(tab_Data[0])):
            minuteNumber = self.utilities_GetMinuteOfDay(tab_Data[0][i][0]) - 1
            if tab_Data[0][i][1] != None:
                tab_Final_SH[minuteNumber] = tab_Final_SH[minuteNumber] + tab_Data[0][i][1]
                tab_Cpt_SH[minuteNumber] = tab_Cpt_SH[minuteNumber] + 1
            if tab_Data[0][i][2] != None:
                tab_Final_DHW[minuteNumber] = tab_Final_DHW[minuteNumber] + tab_Data[0][i][2]
                tab_Cpt_DHW[minuteNumber] = tab_Cpt_DHW[minuteNumber] + 1 
                
        for i in range(0,1440): #1440 = 60*24
            if tab_Cpt_SH[i] != 0:
                tab_Final_SH[i] = (tab_Final_SH[i] / tab_Cpt_SH[i])/1000
            else:
                tab_Final_SH[i] = 0
            if tab_Cpt_DHW[i] != 0:
                tab_Final_DHW[i] = tab_Final_DHW[i] / tab_Cpt_DHW[i]
            else:
                tab_Final_DHW[i] = 0
            
        graph = pandas.DataFrame(tab_Final_DHW, columns=['DHW heat demand (W) - Avg on one day'])#, name="DHW demand, average on days")
        graph['Date'] = pandas.Series(tab_Final_Date)
        graph.plot(x='Date', y='DHW heat demand (W) - Avg on one day')
        if self.autosave:
            matplotlib.pyplot.savefig(self.savePath + "chart_line - DHW Heat demand (W) - Avg on one day")
            matplotlib.pyplot.close()
        else:
            matplotlib.pyplot.show()
        
        graph2 = pandas.DataFrame(tab_Final_SH, columns=["SH heat demand (kW) - Avg on one day"])#, name="DHW demand, average on days")
        graph2['Date'] = pandas.Series(tab_Final_Date)
        graph2.plot(x='Date', y='SH heat demand (kW) - Avg on one day')
        if self.autosave:
            matplotlib.pyplot.savefig(self.savePath + "chart_line - SH Heat demand (W) - Avg on one day")
        else:
            matplotlib.pyplot.show()
           
    def plotLineChart_Elec_AvgDays(self, eDateBegin, eDateEnd):
        self.resetStringAttribute()
        
        tab_Data = self.qry_GetValueElec_Tab(eDateBegin, eDateEnd)
        nbDay = self.utilities_GetNumberOfDays(eDateBegin, eDateEnd)
        tab_Final_Date = []
        tab_ConsumptionCHP = []
        tab_Final_netProduction = []
        tab_Final_exported = []
        tab_Cpt_ConsumptionCHP = []
        tab_Cpt_Prod = []
        tab_Cpt_exported = []
        
        tab_Final_SelfConsumed = []
        
        for i in range(0,nbDay):
            tab_Final_Date.append(eDateBegin + datetime.timedelta(i))
            tab_Final_netProduction.append(0)
            tab_ConsumptionCHP.append(0)
            tab_Final_exported.append(0)
            tab_Cpt_ConsumptionCHP.append(0)
            tab_Cpt_Prod.append(0)
            tab_Cpt_exported.append(0)
            tab_Final_SelfConsumed.append(0)
            
            
        for i in range(0, len(tab_Data[0])):
            dayNumber = self.utilities_GetNumberOfDays(eDateBegin, tab_Data[0][i][0])
            if tab_Data[0][i][1] != None:
                tab_ConsumptionCHP[dayNumber] = tab_ConsumptionCHP[dayNumber] + tab_Data[0][i][1]
                tab_Cpt_ConsumptionCHP[dayNumber] = tab_Cpt_ConsumptionCHP[dayNumber] + 1
            if tab_Data[0][i][1] != None:
                tab_Final_netProduction[dayNumber] = tab_Final_netProduction[dayNumber] + tab_Data[0][i][2]
                tab_Cpt_Prod[dayNumber] = tab_Cpt_Prod[dayNumber] + 1
            if tab_Data[0][i][3] != None:
                tab_Final_exported[dayNumber] = tab_Final_exported[dayNumber] + tab_Data[0][i][3]
                tab_Cpt_exported[dayNumber] = tab_Cpt_exported[dayNumber] + 1
        
        for i in range(0, nbDay):
            if tab_Cpt_ConsumptionCHP[i] != 0:
                tab_ConsumptionCHP[i] = (tab_ConsumptionCHP[i] / tab_Cpt_ConsumptionCHP[i])*24/1000
            else:
                tab_ConsumptionCHP[i] = 0
            if tab_Cpt_Prod[i] != 0:
                tab_Final_netProduction[i] = (tab_Final_netProduction[i] / tab_Cpt_Prod[i])*24/1000
            else:
                tab_Final_netProduction[i] = 0
            if tab_Cpt_exported[i] != 0:
                tab_Final_exported[i] = (tab_Final_exported[i] / tab_Cpt_exported[i])*24/1000
            else:
                tab_Final_exported[i] = 0
                
            tab_Final_SelfConsumed[i] = (tab_Final_netProduction[i] - tab_Final_exported[i])
                
        graph = pandas.DataFrame(tab_Final_netProduction, columns=['CHP electricity production (kWh/day) - Avg on days'])
        graph['Date'] = pandas.Series(tab_Final_Date)
        graph.plot(x='Date', y='CHP electricity production (kWh/day) - Avg on days')
        if self.autosave:
            matplotlib.pyplot.savefig(self.savePath + "chart_line - CHP electricity prod (kWh over day) - Avg on days")
            matplotlib.pyplot.close()
        else:
            matplotlib.pyplot.show()
        
        graph2 = pandas.DataFrame(tab_ConsumptionCHP, columns=['Electricity consumed by CHP (kWh/day) - Avg on days'])
        graph2['Date'] = pandas.Series(tab_Final_Date)
        graph2.plot(x='Date', y='Electricity consumed by CHP (kWh/day) - Avg on days')
        if self.autosave:
            matplotlib.pyplot.savefig(self.savePath + "chart_line - CHP electricity consumption (kWh over day) - Avg on days")
            matplotlib.pyplot.close()
        else:
            matplotlib.pyplot.show()
        
        graph3 = pandas.DataFrame(tab_Final_exported, columns=['Electricity exported (kWh/day) - Avg on days'])
        graph3['Date'] = pandas.Series(tab_Final_Date)
        graph3.plot(x='Date', y='Electricity exported (kWh/day) - Avg on days')
        if self.autosave:
            matplotlib.pyplot.savefig(self.savePath + "chart_line - CHP electricity exported (kWh over day) - Avg on days")
            matplotlib.pyplot.close()
        else:
            matplotlib.pyplot.show()      
                
        graph4 = pandas.DataFrame(tab_Final_SelfConsumed, columns=['Electricity self consumed (kWh/day) - Avg on days'])
        graph4['Date'] = pandas.Series(tab_Final_Date)
        graph4.plot(x='Date', y='Electricity self consumed (kWh/day) - Avg on days')
        if self.autosave:
            matplotlib.pyplot.savefig(self.savePath + "chart_line - CHP electricity self consumed (kWh over day) - Avg on days")
            matplotlib.pyplot.close()
        else:
            matplotlib.pyplot.show()
        
    def plotLineChart_Elec_AvgOnWeekDays(self, eDateBegin, eDateEnd):
        self.resetStringAttribute()
        
        tab_Data = self.qry_GetValueElec_Tab(eDateBegin, eDateEnd)
        tab_Final_Date = []
        tab_ConsumptionCHP = []
        tab_Final_netProduction = []
        tab_Final_exported = []
        tab_Cpt_ConsumptionCHP = []
        tab_Cpt_Prod = []
        tab_Cpt_exported = []
        tab_Final_SelfConsumed = []
        
        tab_Final_Date.append("Monday")
        tab_Final_Date.append("Tuesday")
        tab_Final_Date.append("Wednesday")
        tab_Final_Date.append("Thursday")
        tab_Final_Date.append("Friday")
        tab_Final_Date.append("Saturday")
        tab_Final_Date.append("Sunday")
        
        for i in range(0,7):
            tab_Final_netProduction.append(0)
            tab_ConsumptionCHP.append(0)
            tab_Final_exported.append(0)
            tab_Cpt_ConsumptionCHP.append(0)
            tab_Cpt_Prod.append(0)
            tab_Cpt_exported.append(0)
            tab_Final_SelfConsumed.append(0)
            
        for i in range(0, len(tab_Data[0])):
            dayOfWeek = self.utilities_GetDayOfWeek(tab_Data[0][i][0])
            if tab_Data[0][i][1] != None:
                tab_ConsumptionCHP[dayOfWeek] = tab_ConsumptionCHP[dayOfWeek] + tab_Data[0][i][1]
                tab_Cpt_ConsumptionCHP[dayOfWeek] = tab_Cpt_ConsumptionCHP[dayOfWeek] + 1
            if tab_Data[0][i][1] != None:
                tab_Final_netProduction[dayOfWeek] = tab_Final_netProduction[dayOfWeek] + tab_Data[0][i][2]
                tab_Cpt_Prod[dayOfWeek] = tab_Cpt_Prod[dayOfWeek] + 1
            if tab_Data[0][i][3] != None:
                tab_Final_exported[dayOfWeek] = tab_Final_exported[dayOfWeek] + tab_Data[0][i][3]
                tab_Cpt_exported[dayOfWeek] = tab_Cpt_exported[dayOfWeek] + 1
        
        for i in range(0, 7):
            if tab_Cpt_ConsumptionCHP[i] != 0:
                tab_ConsumptionCHP[i] = (tab_ConsumptionCHP[i] / tab_Cpt_ConsumptionCHP[i])*24/1000
            else:
                tab_ConsumptionCHP[i] = 0
            if tab_Cpt_Prod[i] != 0:
                tab_Final_netProduction[i] = (tab_Final_netProduction[i] / tab_Cpt_Prod[i])*24/1000
            else:
                tab_Final_netProduction[i] = 0
            if tab_Cpt_exported[i] != 0:
                tab_Final_exported[i] = (tab_Final_exported[i] / tab_Cpt_exported[i])*24/1000
            else:
                tab_Final_exported[i] = 0
                
            tab_Final_SelfConsumed[i] = tab_Final_netProduction[i] - tab_Final_exported[i]
                
        graph = pandas.DataFrame(tab_Final_netProduction, columns=['CHP electricity production (kWh/day) - Avg on weekdays'])
        graph['Date'] = pandas.Series(tab_Final_Date)
        graph.plot(x='Date', y='CHP electricity production (kWh/day) - Avg on weekdays')
        if self.autosave:
            matplotlib.pyplot.savefig(self.savePath + "chart_line - CHP electricity prod (kWh over day) - Avg on weekdays")
            matplotlib.pyplot.close()
        else:
            matplotlib.pyplot.show()
        
        graph2 = pandas.DataFrame(tab_ConsumptionCHP, columns=['Electricity consumed by CHP (kWh/day) - Avg on weekdays'])
        graph2['Date'] = pandas.Series(tab_Final_Date)
        graph2.plot(x='Date', y='Electricity consumed by CHP (kWh/day) - Avg on weekdays')
        if self.autosave:
            matplotlib.pyplot.savefig(self.savePath + "chart_line - CHP electricity consumed (kWh over day) - Avg on weekdays")
            matplotlib.pyplot.close()
        else:
            matplotlib.pyplot.show()
        
        graph3 = pandas.DataFrame(tab_Final_exported, columns=['Electricity exported (kWh/day) - Avg on weekdays'])
        graph3['Date'] = pandas.Series(tab_Final_Date)
        graph3.plot(x='Date', y='Electricity exported (kWh/day) - Avg on weekdays')
        if self.autosave:
            matplotlib.pyplot.savefig(self.savePath + "chart_line - CHP electricity exported (kWh over day) - Avg on weekdays")
            matplotlib.pyplot.close()
        else:
            matplotlib.pyplot.show()      
                
        graph4 = pandas.DataFrame(tab_Final_SelfConsumed, columns=['Electricity self consumed (kWh/day) - Avg on weekdays'])
        graph4['Date'] = pandas.Series(tab_Final_Date)
        graph4.plot(x='Date', y='Electricity self consumed (kWh/day) - Avg on weekdays')
        if self.autosave:
            matplotlib.pyplot.savefig(self.savePath + "chart_line - CHP electricity self consumed (kWh over day) - Avg on weekdays")
            matplotlib.pyplot.close()
        else:
            matplotlib.pyplot.show()
            
    def plotLineChart_ElecAvgOnMonth(self, eDateBegin, eDateEnd):
        self.resetStringAttribute()
        
        tab_Data = self.qry_GetValueElec_Tab(eDateBegin, eDateEnd)
        tab_Final_Date = []
        tab_ConsumptionCHP = []
        tab_Final_netProduction = []
        tab_Final_exported = []
        tab_Cpt_ConsumptionCHP = []
        tab_Cpt_Prod = []
        tab_Cpt_exported = []
        tab_Final_SelfConsumed = []
        
        tab_Final_Date.append("January")
        tab_Final_Date.append("February")
        tab_Final_Date.append("March")
        tab_Final_Date.append("April")
        tab_Final_Date.append("May")
        tab_Final_Date.append("June")
        tab_Final_Date.append("July")
        tab_Final_Date.append("August")
        tab_Final_Date.append("September")
        tab_Final_Date.append("October")
        tab_Final_Date.append("November")
        tab_Final_Date.append("December")
        
        for i in range(0,12):
            tab_Final_netProduction.append(0)
            tab_ConsumptionCHP.append(0)
            tab_Final_exported.append(0)
            tab_Cpt_ConsumptionCHP.append(0)
            tab_Cpt_Prod.append(0)
            tab_Cpt_exported.append(0)
            tab_Final_SelfConsumed.append(0)
            
        for i in range(0, len(tab_Data[0])):
            lMonth = self.utilities_GetMonth(tab_Data[0][i][0])
            if tab_Data[0][i][1] != None:
                tab_ConsumptionCHP[lMonth] = tab_ConsumptionCHP[lMonth] + tab_Data[0][i][1]
                tab_Cpt_ConsumptionCHP[lMonth] = tab_Cpt_ConsumptionCHP[lMonth] + 1
            if tab_Data[0][i][1] != None:
                tab_Final_netProduction[lMonth] = tab_Final_netProduction[lMonth] + tab_Data[0][i][2]
                tab_Cpt_Prod[lMonth] = tab_Cpt_Prod[lMonth] + 1
            if tab_Data[0][i][3] != None:
                tab_Final_exported[lMonth] = tab_Final_exported[lMonth] + tab_Data[0][i][3]
                tab_Cpt_exported[lMonth] = tab_Cpt_exported[lMonth] + 1
        
        for i in range(0, 12):
            if tab_Cpt_ConsumptionCHP[i] != 0:
                tab_ConsumptionCHP[i] = (tab_ConsumptionCHP[i] / tab_Cpt_ConsumptionCHP[i])*24*7/1000
            else:
                tab_ConsumptionCHP[i] = 0
            if tab_Cpt_Prod[i] != 0:
                tab_Final_netProduction[i] = (tab_Final_netProduction[i] / tab_Cpt_Prod[i])*24*7/1000
            else:
                tab_Final_netProduction[i] = 0
            if tab_Cpt_exported[i] != 0:
                tab_Final_exported[i] = (tab_Final_exported[i] / tab_Cpt_exported[i])*24*7/1000
            else:
                tab_Final_exported[i] = 0
                
            tab_Final_SelfConsumed[i] = tab_Final_netProduction[i] - tab_Final_exported[i]
                
        graph = pandas.DataFrame(tab_Final_netProduction, columns=['CHP electricity production (kWh/week) - Avg on month'])
        graph['Date'] = pandas.Series(tab_Final_Date)
        graph.plot(x='Date', y='CHP electricity production (kWh/week) - Avg on month')
        if self.autosave:
            matplotlib.pyplot.savefig(self.savePath + "chart_line - CHP electricity production (kWh over week) - Avg on month")
            matplotlib.pyplot.close()
        else:
            matplotlib.pyplot.show()
        
        graph2 = pandas.DataFrame(tab_ConsumptionCHP, columns=['Electricity consumed by CHP (kWh/week) - Avg on month'])
        graph2['Date'] = pandas.Series(tab_Final_Date)
        graph2.plot(x='Date', y='Electricity consumed by CHP (kWh/week) - Avg on month')
        if self.autosave:
            matplotlib.pyplot.savefig(self.savePath + "chart_line - CHP electricity consumed by CHP (kWh over week) - Avg on month")
            matplotlib.pyplot.close()
        else:
            matplotlib.pyplot.show() 
        
        graph3 = pandas.DataFrame(tab_Final_exported, columns=['Electricity exported (kWh/week) - Avg on month'])
        graph3['Date'] = pandas.Series(tab_Final_Date)
        graph3.plot(x='Date', y='Electricity exported (kWh/week) - Avg on month')
        if self.autosave:
            matplotlib.pyplot.savefig(self.savePath + "chart_line - Electricity exported (kWh over week) - Avg on month")
            matplotlib.pyplot.close()
        else:
            matplotlib.pyplot.show()   
                
        graph4 = pandas.DataFrame(tab_Final_SelfConsumed, columns=['Electricity self consumed (kWh/week) - Avg on month'])
        graph4['Date'] = pandas.Series(tab_Final_Date)
        graph4.plot(x='Date', y='Electricity self consumed (kWh/week) - Avg on month')
        if self.autosave:
            matplotlib.pyplot.savefig(self.savePath + "chart_line - Electricity self consumed (kWh over week) - Avg on month")
            matplotlib.pyplot.close()
        else:
            matplotlib.pyplot.show()
  
    def plotLineChart_ElecAvgOnOneDay(self, eDateBegin, eDateEnd):
        self.resetStringAttribute()
        
        tab_Data = self.qry_GetValueElec_Tab(eDateBegin, eDateEnd)
        tab_Final_Date = []
        tab_ConsumptionCHP = []
        tab_Final_netProduction = []
        tab_Final_exported = []
        tab_Cpt_ConsumptionCHP = []
        tab_Cpt_Prod = []
        tab_Cpt_Exported = []
        tab_Final_SelfConsumed = []
        
        
        for i in range(0,1440): #60*24 =1440 minutes
            tab_Final_Date.append(datetime.time(i//60, i%60))
            tab_Final_netProduction.append(0)
            tab_ConsumptionCHP.append(0)
            tab_Final_exported.append(0)
            tab_Cpt_ConsumptionCHP.append(0)
            tab_Cpt_Prod.append(0)
            tab_Cpt_Exported.append(0)
            tab_Final_SelfConsumed.append(0)
            
        for i in range(0, len(tab_Data[0])):
            lMinute = self.utilities_GetMinuteOfDay(tab_Data[0][i][0])
            if tab_Data[0][i][1] != None:
                tab_ConsumptionCHP[lMinute] = tab_ConsumptionCHP[lMinute] + tab_Data[0][i][1]
                tab_Cpt_ConsumptionCHP[lMinute] = tab_Cpt_ConsumptionCHP[lMinute] + 1
            if tab_Data[0][i][1] != None:
                tab_Final_netProduction[lMinute] = tab_Final_netProduction[lMinute] + tab_Data[0][i][2]
                tab_Cpt_Prod[lMinute] = tab_Cpt_Prod[lMinute] + 1
            if tab_Data[0][i][3] != None:
                tab_Final_exported[lMinute] = tab_Final_exported[lMinute] + tab_Data[0][i][3]
                tab_Cpt_Exported[lMinute] = tab_Cpt_Exported[lMinute] + 1
        
        for i in range(0,1440): #60*24 =1440 minutes
            if tab_Cpt_ConsumptionCHP[i] != 0:
                tab_ConsumptionCHP[i] = tab_ConsumptionCHP[i] / tab_Cpt_ConsumptionCHP[i]
            else:
                tab_ConsumptionCHP[i] = 0
            if tab_Cpt_Prod[i] != 0:
                tab_Final_netProduction[i] = tab_Final_netProduction[i] / tab_Cpt_Prod[i]
            else:
                tab_Final_netProduction[i] = 0
            if tab_Cpt_Exported[i] != 0:
                tab_Final_exported[i] = tab_Final_exported[i] / tab_Cpt_Exported[i]
            else:
                tab_Final_exported[i] = 0
                
            tab_Final_SelfConsumed[i] = tab_Final_netProduction[i] - tab_Final_exported[i]
                
        graph = pandas.DataFrame(tab_Final_netProduction, columns=['CHP electricity production (W) - Avg on one day'])
        graph['Date'] = pandas.Series(tab_Final_Date)
        graph.plot(x='Date', y='CHP electricity production (W) - Avg on one day')
        if self.autosave:
            matplotlib.pyplot.savefig(self.savePath + "chart_line - CHP electricity production (W) - Avg on one day")
            matplotlib.pyplot.close()
        else:
            matplotlib.pyplot.show()
        
        graph2 = pandas.DataFrame(tab_ConsumptionCHP, columns=['Electricity consumed by CHP (W) - Avg on one day'])
        graph2['Date'] = pandas.Series(tab_Final_Date)
        graph2.plot(x='Date', y='Electricity consumed by CHP (W) - Avg on one day')
        if self.autosave:
            matplotlib.pyplot.savefig(self.savePath + "chart_line - Electricity consumed by CHP (W) - Avg on one day")
            matplotlib.pyplot.close()
        else:
            matplotlib.pyplot.show()
        
        graph3 = pandas.DataFrame(tab_Final_exported, columns=['Electricity exported (W) - Avg on one day'])
        graph3['Date'] = pandas.Series(tab_Final_Date)
        graph3.plot(x='Date', y='Electricity exported (W) - Avg on one day')
        if self.autosave:
            matplotlib.pyplot.savefig(self.savePath + "chart_line - Electricity exported (W) - Avg on one day")
            matplotlib.pyplot.close()
        else:
            matplotlib.pyplot.show()    
                
        graph4 = pandas.DataFrame(tab_Final_SelfConsumed, columns=['Electricity self consumed (W) - Avg on one day'])
        graph4['Date'] = pandas.Series(tab_Final_Date)
        graph4.plot(x='Date', y='Electricity self consumed (W) - Avg on one day')
        if self.autosave:
            matplotlib.pyplot.savefig(self.savePath + "chart_line - Electricity self consumed (W) - Avg on one day")
            matplotlib.pyplot.close()
        else:
            matplotlib.pyplot.show()
    
    def plotLineChart_SolarProductionAvgDays(self, eDateBegin, eDateEnd):
        self.resetStringAttribute()
        
        tab_Data = self.qry_GetSolarProductionGross_Tab(eDateBegin, eDateEnd)
        nbDay = self.utilities_GetNumberOfDays(eDateBegin, eDateEnd)
        tab_Final_Date = []
        tab_Final_SolarProd = []
        tab_Cpt_SolarProd = []
        
        for i in range(0,nbDay):
            tab_Final_Date.append(eDateBegin + datetime.timedelta(i))
            tab_Final_SolarProd.append(0)
            tab_Cpt_SolarProd.append(0)
        
        for i in range(0,len(tab_Data[0])):
            dayNumber = self.utilities_GetNumberOfDays(eDateBegin, tab_Data[0][i][0])
            
            if tab_Data[0][i][1] != None:
                if tab_Data[0][i][1] < 10000: #Because of sensor strange behavior
                    tab_Final_SolarProd[dayNumber] = tab_Final_SolarProd[dayNumber] + tab_Data[0][i][1]
                tab_Cpt_SolarProd[dayNumber] = tab_Cpt_SolarProd[dayNumber] + 1
            
        for i in range(0,nbDay):
            if tab_Cpt_SolarProd[i] != 0:
                tab_Final_SolarProd[i] = (tab_Final_SolarProd[i]/tab_Cpt_SolarProd[i])*24/1000
            else:
                tab_Final_SolarProd[i] = 0
        
        graph = pandas.DataFrame(tab_Final_SolarProd, columns=['Solar heat production (gross) (kWh/day) - Avg on days'])#, name="DHW demand, average on days")
        graph['Date'] = pandas.Series(tab_Final_Date)
        graph.plot(x='Date', y='Solar heat production (gross) (kWh/day) - Avg on days')
        if self.autosave:
            matplotlib.pyplot.savefig(self.savePath + "chart_line - Solar gross heat production (kWh over day) - Avg on days")
            matplotlib.pyplot.close()
        else:
            matplotlib.pyplot.show()
        
    def plotLineChart_SolarProductionAvgOnMonth(self, eDateBegin, eDateEnd):
        self.resetStringAttribute()
        
        tab_Data = self.qry_GetSolarProductionGross_Tab(eDateBegin, eDateEnd)

        tab_Final_Date = []
        tab_Final_SolarProd = []
        tab_Cpt_SolarProd = []
        
        tab_Final_Date.append("January")
        tab_Final_Date.append("February")
        tab_Final_Date.append("March")
        tab_Final_Date.append("April")
        tab_Final_Date.append("May")
        tab_Final_Date.append("June")
        tab_Final_Date.append("July")
        tab_Final_Date.append("August")
        tab_Final_Date.append("September")
        tab_Final_Date.append("October")
        tab_Final_Date.append("November")
        tab_Final_Date.append("December")
        for i in range(0,12):
            tab_Final_SolarProd.append(0)
            tab_Cpt_SolarProd.append(0)
        
        for i in range(0,len(tab_Data[0])):
            monthNumber = self.utilities_GetMonth(tab_Data[0][i][0])
            if tab_Data[0][i][1] != None:
                if tab_Data[0][i][1] < 10000: #Because of sensor strange behavior
                    tab_Final_SolarProd[monthNumber] = tab_Final_SolarProd[monthNumber] + tab_Data[0][i][1]
                tab_Cpt_SolarProd[monthNumber] = tab_Cpt_SolarProd[monthNumber] + 1
                
        for i in range(0,12):
            if tab_Cpt_SolarProd[i] != 0:
                tab_Final_SolarProd[i] = (tab_Final_SolarProd[i] / tab_Cpt_SolarProd[i])*24*7/1000
            else:
                tab_Final_SolarProd[i] = 0
            
        graph = pandas.DataFrame(tab_Final_SolarProd, columns=['Solar heat production (kWh/week) - Avg on Month'])#, name="DHW demand, average on days")
        graph['Date'] = pandas.Series(tab_Final_Date)
        graph.plot(x='Date', y='Solar heat production (kWh/week) - Avg on Month')
        if self.autosave:
            matplotlib.pyplot.savefig(self.savePath + "chart_line - Solar gross heat production (kWh over week) - Avg on Month")
            matplotlib.pyplot.close()
        else:
            matplotlib.pyplot.show()

    def plotLineChart_SolarProductionAvgOnOneDay(self, eDateBegin, eDateEnd):
        self.resetStringAttribute()
        
        tab_Data = self.qry_GetSolarProductionGross_Tab(eDateBegin, eDateEnd)
        
        tab_Final_Date = []
        tab_Final_SolarProd = []
        tab_Cpt_SolarProd = []
        
        for i in range(0,1440):
            tab_Final_Date.append(datetime.time(i//60, i % 60))
            tab_Final_SolarProd.append(0)
            tab_Cpt_SolarProd.append(0)
        
        for i in range(0,len(tab_Data[0])):
            minuteNumber = self.utilities_GetMinuteOfDay(tab_Data[0][i][0]) - 1
            if tab_Data[0][i][1] != None:
                if tab_Data[0][i][1] < 10000: #Because of sensor strange behavior
                    tab_Final_SolarProd[minuteNumber] = tab_Final_SolarProd[minuteNumber] + tab_Data[0][i][1]
                tab_Cpt_SolarProd[minuteNumber] = tab_Cpt_SolarProd[minuteNumber] + 1
                
        for i in range(0,1440): #1440 = 60*24
            if tab_Cpt_SolarProd[i] != 0:
                tab_Final_SolarProd[i] = tab_Final_SolarProd[i] / tab_Cpt_SolarProd[i]
            else:
                tab_Final_SolarProd[i] = 0
            
        graph = pandas.DataFrame(tab_Final_SolarProd, columns=['Solar heat production (W) - Avg on one day'])#, name="DHW demand, average on days")
        graph['Date'] = pandas.Series(tab_Final_Date)
        graph.plot(x='Date', y='Solar heat production (W) - Avg on one day')
        if self.autosave:
            matplotlib.pyplot.savefig(self.savePath + "chart_line - Solar gross heat production (W) - Avg on one day")
            matplotlib.pyplot.close()
        else:
            matplotlib.pyplot.show()

    def plotLineChart_CHP_DeltaT(self, eDateBegin, eDateEnd):
        self.resetStringAttribute()
        
        tab_Data = self.qry_GetCHP_DeltaT_Tab(eDateBegin, eDateEnd)
        nbDay = self.utilities_GetNumberOfDays(eDateBegin, eDateEnd)
        
        tab_Final_Date = []
        tab_Final_deltaT = []
        tab_Cpt_deltaT = []
        
        for i in range(0,nbDay):
            tab_Final_Date.append(eDateBegin + datetime.timedelta(i))
            tab_Final_deltaT.append(0)
            tab_Cpt_deltaT.append(0)
        
        for i in range(0, len(tab_Data[0])):
            dayNumber = self.utilities_GetNumberOfDays(eDateBegin, tab_Data[0][i][0])
            if tab_Data[0][i][1] != None and float(tab_Data[0][i][1]) > 0:
                tab_Final_deltaT[dayNumber] = tab_Final_deltaT[dayNumber] + tab_Data[0][i][1]
                tab_Cpt_deltaT[dayNumber] = tab_Cpt_deltaT[dayNumber] + 1
        
        for i in range(0, nbDay):
            if tab_Cpt_deltaT[i] != 0:
                tab_Final_deltaT[i] = tab_Final_deltaT[i] / tab_Cpt_deltaT[i]
            else:
                tab_Final_deltaT[i] = 0   

        graph = pandas.DataFrame(tab_Final_deltaT, columns=['CHP difference temperature between flow and return (Celsius) - Avg on days (only when SH working)'])
        graph['Date'] = pandas.Series(tab_Final_Date)
        graph.plot(x='Date', y='CHP difference temperature between flow and return (Celsius) - Avg on days (only when SH working)')
        if self.autosave:
            matplotlib.pyplot.savefig(self.savePath + "chart_line - CHP difference temperature between flow and return (Celsius) - Avg on days")
            matplotlib.pyplot.close()
        else:
            matplotlib.pyplot.show()

    def plotScatter_Sigmoid_SH_AvgOnDays(self, eDateBegin, eDateEnd, eNbDaysAvg):
        self.resetStringAttribute()
        nbDayAvg = eNbDaysAvg
        
        tabData = self.qry_GetDataForSigmoidSH(eDateBegin, eDateEnd)
        
        tab_temporary_Temp = []
        tab_temporary_SHdemand = []
        tab_cpt = []
        
        tab_Final_Temp = []
        tab_Final_SHdemand = []
        
        nbDay = self.utilities_GetNumberOfDays(eDateBegin, eDateEnd)
        
        for i in range(0,nbDay):
            tab_Final_Temp.append(0)
            tab_Final_SHdemand.append(0)
            tab_temporary_Temp.append(0)
            tab_temporary_SHdemand.append(0)
            tab_cpt.append(0)
            
        for i in range(0,len(tabData[0])):
            dayNumber = self.utilities_GetNumberOfDays(eDateBegin, tabData[0][i][0])
            if tabData[0][i][1] != None and tabData[0][i][2] != None:
                tab_temporary_SHdemand[dayNumber] = tab_temporary_SHdemand[dayNumber] + tabData[0][i][1]
                tab_temporary_Temp[dayNumber] = tab_temporary_Temp[dayNumber] + tabData[0][i][2]
                tab_cpt[dayNumber] = tab_cpt[dayNumber] + 1
         
        
        for i in range(0, nbDay):
            if tab_cpt[i] != 0:
                tab_temporary_SHdemand[i] = tab_temporary_SHdemand[i] / tab_cpt[i]
                tab_temporary_Temp[i] = tab_temporary_Temp[i] / tab_cpt[i]
            else:
                tab_temporary_SHdemand[i] = 0  
                tab_temporary_Temp[i] = 0 

        for i in range(nbDayAvg, nbDay):
            nbMinus = 0
            for j in range(0,nbDayAvg):
                if tab_temporary_SHdemand[i-j] != 0 and tab_temporary_Temp[i-j] != 0:
                    tab_Final_SHdemand[i] = tab_Final_SHdemand[i] + tab_temporary_SHdemand[i-j]
                    tab_Final_Temp[i] = tab_Final_Temp[i] + tab_temporary_Temp[i-j]
                else:
                    nbMinus = nbMinus +1
            if nbMinus < nbDayAvg:
                tab_Final_SHdemand[i] = round(tab_Final_SHdemand[i] / (nbDayAvg-nbMinus),1)
                tab_Final_Temp[i] = round(tab_Final_Temp[i] / (nbDayAvg-nbMinus),1)
            else:
                tab_Final_SHdemand[i] = tab_temporary_SHdemand[i]
                tab_Final_Temp[i] = tab_temporary_Temp[i]
            
        cpt =0
        for i in range(0,nbDay):
            if tab_Final_SHdemand[i] == 0 and tab_Final_Temp[i] == 0:
                cpt = cpt +1
        print('Nombre de (0,0) : ' + str(cpt))
        
        graph = pandas.DataFrame(tab_Final_SHdemand, columns=['SH demand as a function of external temperature - Avg on ' + str(nbDayAvg) + ' days'])
        graph['External temperature'] = pandas.Series(tab_Final_Temp)
        graph.plot(kind='scatter', x= 'External temperature', y='SH demand as a function of external temperature - Avg on ' + str(nbDayAvg) + ' days')
        if self.autosave:
            matplotlib.pyplot.savefig(self.savePath + "\\chart_Scatter - Sigmoid plot - SH demand as a function of external temperature - Avg on " + str(nbDayAvg) + " days'")
            matplotlib.pyplot.close()
        else:
            matplotlib.pyplot.show()



    def get_HeatProduction_Console(self, eDateBegin, eDateEnd):  
        self.resetStringAttribute()
        
        #dedicated production
        prod_CHPtoDHW = self.qry_EnergyCHPtoDHW(eDateBegin, eDateEnd)
        prod_CHPtoSH = self.qry_EnergyCHPtoSH(eDateBegin, eDateEnd)
        # Gross production
        production_Solar_Gross = round(self.qry_EnergySolarPanels(eDateBegin, eDateEnd), 1)
        production_Boilers_Gross = round(self.qry_EnergyBoilers(eDateBegin, eDateEnd),1)
        production_CHP_Gross = round(prod_CHPtoDHW + prod_CHPtoSH,1)
        total_production = round(production_Solar_Gross + production_Boilers_Gross + production_CHP_Gross,1)
        #Percent
        if self.lTexte == "Yes":
            production_Solar_Gross_Percent = round((production_Solar_Gross/total_production)*100,1)
            production_Boilers_Gross_Percent = round((production_Boilers_Gross/total_production)*100,1)
            production_CHP_Gross_Percent = round((production_CHP_Gross/total_production)*100,1)
        
        textToPrint = chr(10)
        textToPrint = textToPrint + "PRODUCTION :" + chr(10)
        if self.lTexte == "Yes":
            self.beCarefulOfDataHealth()
            textToPrint = textToPrint + "Between " + eDateBegin.strftime("%Y-%m-%d %H:%M:%S") + " and " + eDateEnd.strftime("%Y-%m-%d %H:%M:%S") + chr(10)
            textToPrint = textToPrint + "Provided by solar : " + str(production_Solar_Gross) + " kWh (" + str(production_Solar_Gross_Percent) + " % of energy production)" + chr(10)
            textToPrint = textToPrint + "Produced by CHP : " + str(production_CHP_Gross) + " kWh (" + str(production_CHP_Gross_Percent) + " % of energy production)" + chr(10)
            textToPrint = textToPrint + "Produced by Boilers : " + str(production_Boilers_Gross) + " kWh (" + str(production_Boilers_Gross_Percent) + " % of energy production)" + chr(10)
        else:
            textToPrint = textToPrint + "Production parameters can not be calculated at this dates because of data missing on DataBase" + chr(10)
            textToPrint = textToPrint + "Choose an other date, and retry" + chr(10)
            textToPrint = textToPrint + "Error : " + self.lTexte + chr(10)
        
        if self.autosave:
            self.utilities_AddingOnTextFile(self.savePath + "\\Summary.txt", textToPrint)
        else:
            print(textToPrint)
            
    def get_HeatConsumption_Console(self, eDateBegin, eDateEnd):
        self.resetStringAttribute()
        
        #consumption
        recirculation_DHW= round(self.qry_EnergyConsumedInDHWRecirculated(eDateBegin, eDateEnd),1)
        demand_SH = round(self.qry_EnergyConsumptionForSH(eDateBegin, eDateEnd),1)
        demand_DHW = round(self.qry_EnergyConsumptionForDHW(eDateBegin, eDateEnd),1)
        total_demand = demand_DHW + demand_SH
        
        # intern movement
        transfert_SHtoDHW = round(self.qry_EnergyTakenFromSHtoDHW(eDateBegin, eDateEnd),1)
        #Percent
        recirculation_DHW_Percent = round((recirculation_DHW/total_demand)*100,1)
        
        if self.lTexte == "Yes":
            demand_SH_Percent = round((demand_SH/total_demand)*100,1)
            demand_DHW_Percent = round((demand_DHW/total_demand)*100,1)
        
        textToPrint = chr(10)
        textToPrint = textToPrint + "CONSUMPTION :" + chr(10)
        if self.lTexte == "Yes":
            self.beCarefulOfDataHealth()
            textToPrint = textToPrint + "Between " + eDateBegin.strftime("%Y-%m-%d %H:%M:%S") + " and " + eDateEnd.strftime("%Y-%m-%d %H:%M:%S") + chr(10)
            textToPrint = textToPrint + "Consumed for SH : " + str(demand_SH) + " kWh (" + str(demand_SH_Percent) + " % of energy demand)" + chr(10)
            textToPrint = textToPrint + "Consumed for DHW : " + str(demand_DHW) + " kWh (" + str(round(demand_DHW_Percent)) + " % of energy demand)" + chr(10)
            textToPrint = textToPrint + "Consumed in DHW recirculation : " + str(recirculation_DHW) + " kWh (" + str(recirculation_DHW_Percent) + " % of energy losses)" + chr(10)
            textToPrint = textToPrint + "Transfered from SH to DHW : "+ str(transfert_SHtoDHW) + " kWh" + chr(10)
        else:
            textToPrint = textToPrint + "Consumption parameters can not be calculated at this dates because of data missing on DataBase" + chr(10)
            textToPrint = textToPrint + "Choose an other date, and retry" + chr(10)
            textToPrint = textToPrint + "Error : " + self.lTexte + chr(10)
            
        if self.autosave:
            self.utilities_AddingOnTextFile(self.savePath + "\\Summary.txt", textToPrint)
        else:
            print(textToPrint)
         
    def get_HeatLosses_Console(self, eDateBegin, eDateEnd):
        self.resetStringAttribute()
        
        # production
        production_Solar = round(self.qry_EnergySolarPanels(eDateBegin, eDateEnd),1)
        production_Boilers = round(self.qry_EnergyBoilers(eDateBegin, eDateEnd),1)
        prod_BoilerToSH = round(self.qry_EnergyProvidedToSHonlyByBoilers(eDateBegin, eDateEnd),1)
        prod_CHPtoSH = round(self.qry_EnergyCHPtoSH(eDateBegin, eDateEnd),1)
        prod_CHPtoDHW = round(self.qry_EnergyCHPtoDHW(eDateBegin, eDateEnd),1)
        production_CHP = prod_CHPtoSH + prod_CHPtoDHW
        # intern movement
        transfert_SHtoDHW = round(self.qry_EnergyTakenFromSHtoDHW(eDateBegin, eDateEnd),1)
        # net production
        production_SolarNet = round(self.qry_EnergySolarPanelNET(eDateBegin, eDateEnd),1)
        #consumption
        demand_SH = round(self.qry_EnergyConsumptionForSH(eDateBegin, eDateEnd),1)
        demand_DHW = round(self.qry_EnergyConsumptionForDHW(eDateBegin, eDateEnd),1)
        recirculation_DHW= round(self.qry_EnergyConsumedInDHWRecirculated(eDateBegin, eDateEnd),1)
        #total
        total_EnergyProduced = production_Solar + production_Boilers + production_CHP
        total_EnergyDemand = demand_SH + demand_DHW + recirculation_DHW
        total_Losses = total_EnergyProduced - total_EnergyDemand
        #LOSSES
        losses_SolarSystem = production_Solar - production_SolarNet
        losses_DHW_System = production_SolarNet + prod_CHPtoDHW + transfert_SHtoDHW - demand_DHW - recirculation_DHW
        losses_SH_System = prod_BoilerToSH + prod_CHPtoSH - demand_SH 

        if self.lTexte == "Yes":
            total_losses_Percent = round((total_Losses/total_EnergyProduced)*100,1)
            losses_SolarSystem_Percent = round((losses_SolarSystem/total_Losses)*100,1)
            losses_DHW_System_Percent = round((losses_DHW_System/total_Losses)*100,1)
            losses_SH_System_Percent = round((losses_SH_System/total_Losses)*100,1)
        
        textToPrint = chr(10)
        textToPrint = textToPrint + "LOSSES :" + chr(10)
        if self.lTexte == "Yes":
            self.beCarefulOfDataHealth()
            textToPrint = textToPrint + "Between " + eDateBegin.strftime("%Y-%m-%d %H:%M:%S") + " and " + eDateEnd.strftime("%Y-%m-%d %H:%M:%S") + chr(10)
            textToPrint = textToPrint + "Total Losses : " + str(total_Losses) + " kWh (" + str(total_losses_Percent) + " % of energy production)" + chr(10)
            textToPrint = textToPrint + "Lost in the solar system : " + str(losses_SolarSystem) + " kWh (" + str(losses_SolarSystem_Percent) + " % of energy losses)" + chr(10)
            textToPrint = textToPrint + "Lost in DHW system : "+ str(losses_DHW_System) + " kWh (" + str(losses_DHW_System_Percent) + " % of energy losses)" + chr(10)
            textToPrint = textToPrint + "Lost in SH system : " +str(losses_SH_System) + " kWh (" + str(losses_SH_System_Percent) + " % of energy losses)" + chr(10)
        else:
            textToPrint = textToPrint + "Losses can not be calculated at this dates because of data missing on DataBase" + chr(10)
            textToPrint = textToPrint + "Choose an other date, and retry" + chr(10)
            textToPrint = textToPrint + "Error : " + self.lTexte + chr(10)
        
        if self.autosave:
            self.utilities_AddingOnTextFile(self.savePath + "\\Summary.txt", textToPrint)
        else:
            print(textToPrint)
           
    def get_ElecBalance_Console(self, eDateBegin, eDateEnd):
        self.resetStringAttribute()
        
        #Get electrical parameter
        elec_CHPproduction = self.qry_GetElecProducedByCHP(eDateBegin, eDateEnd)
        elec_ConsumedForCHP = self.qry_GetElecConsumedByCHP(eDateBegin, eDateEnd)
        elec_exported = self.qry_GetElecExported(eDateBegin, eDateEnd)
        elec_SelfConsumption = elec_CHPproduction - elec_exported
        #Percent
        rate_EnergyNeededAboveHisProduction = round((elec_ConsumedForCHP/elec_CHPproduction)*100,1)
        rate_SelfConsumption = round((elec_SelfConsumption/elec_CHPproduction)*100,1)
        
        textToPrint = chr(10)
        textToPrint = textToPrint + "ELECTRICAL BALANCE :" + chr(10)
        if self.lTexte == "Yes":
            self.beCarefulOfDataHealth()
            textToPrint = textToPrint + "Between " + eDateBegin.strftime("%Y-%m-%d %H:%M:%S") + " and " + eDateEnd.strftime("%Y-%m-%d %H:%M:%S") + chr(10)
            textToPrint = textToPrint + "Electricity produced by CHP : " + str(elec_CHPproduction) + " kWh" + chr(10)
            textToPrint = textToPrint + "Electricity needed for the CHP : " + str(elec_ConsumedForCHP) + " kWh (" + str(rate_EnergyNeededAboveHisProduction) + "% of his production)" + chr(10)
            textToPrint = textToPrint + "Electricity exported : " + str(elec_exported) + " kWh" + chr(10)
            textToPrint = textToPrint + "Self consumed electricity : " + str(elec_SelfConsumption) + " kWh (" + str(rate_SelfConsumption) + " %)" + chr(10)
        else:
            textToPrint = textToPrint + "Electrical balance can not be calculated at this dates, because of data missing on DataBase" + chr(10)
            textToPrint = textToPrint + "Choose an other date, and retry" + chr(10)
            textToPrint = textToPrint + "Error : " + self.lTexte + chr(10)
            
        if self.autosave:
            self.utilities_AddingOnTextFile(self.savePath + "\\Summary.txt", textToPrint)
        else:
            print(textToPrint)
            
    def get_Efficiency_Console(self, eDateBegin, eDateEnd):
        self.resetStringAttribute()
        
        #Boiler 1 Efficiency
        production_boiler1 = round(self.qry_EnergyBoiler1(eDateBegin, eDateEnd),1)
        maxEnergy_Boil1 = round(self.qry_GetGasConsumption_Boil1(dateBegin, dateEnd) * self.gasPCS,1)
        if self.lTexte != "Yes":
            maxEnergy_Boil1 = 1
        efficiency_Boiler1 = round((production_boiler1/maxEnergy_Boil1)*100,1)
        #Boiler 2 
        production_boiler2 = round(self.qry_EnergyBoiler2(eDateBegin, eDateEnd),1)
        maxEnergy_Boil2 = round(self.qry_GetGasConsumption_Boil2(dateBegin, dateEnd) * self.gasPCS,1)
        if self.lTexte != "Yes":
            maxEnergy_Boil2 = 1
        efficiency_Boiler2 = round((production_boiler2/maxEnergy_Boil2)*100,1)
        #CHP Efficiency
        production_CHP = round(self.qry_EnergyCHPtoDHW(eDateBegin, eDateEnd) + self.qry_EnergyCHPtoSH(eDateBegin, eDateEnd),1)
        consumption_CHP_elec = round(self.qry_GetElecConsumedByCHP(dateBegin, dateEnd),1)
        production_CHP_elec = round(self.qry_GetElecProducedByCHP(dateBegin, dateEnd),1)
        maxEnergy_CHP = consumption_CHP_elec + round((self.qry_GetGasConsumption_CHP(dateBegin, dateEnd) * self.gasPCS),1)
        if self.lTexte != "Yes":
            maxEnergy_CHP = 1
        efficiency_CHP = round(((production_CHP_elec + production_CHP)/maxEnergy_CHP)*100,1)
        #Solar efficiency
        production_Solar_Gross = round(self.qry_EnergySolarPanels(eDateBegin, eDateEnd), 1)
        maxEnergy_Solar = 0
        begin_Month = dateBegin.month
        end_Month = dateEnd.month
        for i in range(begin_Month, end_Month):
            maxEnergy_Solar = maxEnergy_Solar + (self.solarRadiation[i]*self.panelArea)
        if begin_Month == end_Month:
            maxEnergy_Solar = self.solarRadiation[begin_Month]*self.panelArea
        efficiency_solar = round((production_Solar_Gross/maxEnergy_Solar)*100,1)
        
        textToPrint = chr(10)
        textToPrint = textToPrint + "EFFICIENCIES :" + chr(10)        
        if self.lTexte == "Yes":
            self.beCarefulOfDataHealth()
            textToPrint = textToPrint + "Between " + eDateBegin.strftime("%Y-%m-%d %H:%M:%S") + " and " + eDateEnd.strftime("%Y-%m-%d %H:%M:%S") + chr(10)   
            textToPrint = textToPrint + "Boiler1 efficiency : " + str(efficiency_Boiler1) + " %" + chr(10)   
            textToPrint = textToPrint + "Boiler2 efficiency : " + str(efficiency_Boiler2) + " %" + chr(10)   
            textToPrint = textToPrint + "CHP efficiency : " + str(efficiency_CHP) + " %" + chr(10)   
            textToPrint = textToPrint + "Solar efficiency : " + str(efficiency_solar) + " %" + chr(10)   
        else:
            textToPrint = textToPrint + "Efficiencies can not be calculated at this dates because of data missing on DataBase" + chr(10)   
            textToPrint = textToPrint + "Choose an other date, and retry" + chr(10)   
            textToPrint = textToPrint + "Error : " + self.lTexte + chr(10)
        
        if self.autosave:
            self.utilities_AddingOnTextFile(self.savePath + "\\Summary.txt", textToPrint)
        else:
            print(textToPrint)   
            
    def get_All_Console(self, eDateBegin, eDateEnd):
        self.get_HeatProduction_Console(eDateBegin, eDateEnd)
        self.get_HeatConsumption_Console(eDateBegin, eDateEnd)
        self.get_HeatLosses_Console(eDateBegin, eDateEnd)
        self.get_ElecBalance_Console(eDateBegin, eDateEnd)
        self.get_Efficiency_Console(eDateBegin, eDateEnd)



obj = Report("eifero249\\griessbaum", "GbN43-m!")

dateBegin = datetime.datetime(2015,8,18)
dateEnd = datetime.datetime(2016,8,18)

savePATH = "mandelieu/"

#savePATH = savePATH + "Report_from_" + str(dateBegin.strftime("%Y-%m-%d")) + "_to_" + str(dateEnd.strftime("%Y-%m-%d"))

obj.createReport(dateBegin, dateEnd, savePATH)





#sensorList = []
#sensorList.append("dp12.dk510_5")
#sensorList.append("dp12.dk510_6")
#obj.utilities_GenericPlots(sensorList, dateBegin, dateEnd)
#obj.utilities_GenericPlot("11", "390_5",dateBegin, dateEnd)

#PARAMETER :
#obj.get_HeatProduction_Console(dateBegin, dateEnd)
#obj.get_HeatConsumption_Console(dateBegin, dateEnd)
#obj.get_HeatLosses_Console(dateBegin, dateEnd)
#obj.get_ElecBalance_Console(dateBegin, dateEnd)
#obj.get_Efficiency_Console(dateBegin, dateEnd)
#obj.get_All_Console(dateBegin, dateEnd)

#PLOT PIE :
#obj.plotPie_GrossProduction(dateBegin, dateEnd)
#obj.plotPie_EnergyRepartition(dateBegin, dateEnd)
#obj.plotPie_HeatDemand(dateBegin, dateEnd)
#obj.plotPie_HeatForDHW(dateBegin, dateEnd)
#obj.plotPie_HeatForSH(dateBegin, dateEnd)
#obj.plotPie_Losses(dateBegin, dateEnd)
#obj.plotPie_AllEnergy(dateBegin, dateEnd)

#PLOT LINE CHART
#obj.plotLineChart_Elec_AvgDays(dateBegin, dateEnd)
#obj.plotLineChart_Elec_AvgOnWeekDays(dateBegin, dateEnd)
#obj.plotLineChart_ElecAvgOnMonth(dateBegin, dateEnd)
#obj.plotLineChart_ElecAvgOnOneDay(dateBegin, dateEnd)
#obj.plotLineChart_HeatDemand_AvgDays(dateBegin, dateEnd)
#obj.plotLineChart_HeatDemand_AvgOnMonth(dateBegin, dateEnd)
#obj.plotLineChart_HeatDemand_AvgOnOneDay(dateBegin, dateEnd)
#obj.plotLineChart_HeatDemand_AvgOnWeekDay(dateBegin, dateEnd)
#obj.plotLineChart_SolarProductionAvgDays(dateBegin, dateEnd)
#obj.plotLineChart_SolarProductionAvgOnMonth(dateBegin, dateEnd)
#obj.plotLineChart_SolarProductionAvgOnOneDay(dateBegin, dateEnd)
#obj.plotLineChart_CHP_DeltaT(dateBegin, dateEnd)

#PLOT SCATTER
#obj.plotScatter_Sigmoid_SH_AvgOnDays(dateBegin, dateEnd, 20)


obj.endClass()


