import database
import datetime
import matplotlib.pyplot as plt

plantNum = 8

db_elec = database.Electricity(plantNum)
db_therm = database.Thermal(plantNum)
db_gas = database.GasDemand(plantNum)

startDate = db_elec.newest_elexp()
print(startDate)
startDate = datetime.datetime(2016,1,27)
endDate = datetime.datetime(2016,6,15)                

db_elec.setStart(startDate)
db_elec.setEnd(endDate)
db_therm.setStart(startDate)
db_therm.setEnd(endDate)
db_gas.setStart(startDate)
db_gas.setEnd(endDate)


db_elec.fetchData()
db_therm.fetchData()
db_gas.fetchData()


def replace_first(column):
    serie = db_elec.df[column]
    min_nonnan = min(serie.dropna())    
    ts_min_nonnan = min(serie[serie==min_nonnan].index)
    sane_duration = (serie.index[-1] - ts_min_nonnan).total_seconds()
    deltay = serie[-1] - min_nonnan
    missing_duration = (ts_min_nonnan - serie.index[0]).total_seconds()
    first_val = min_nonnan - missing_duration*deltay/sane_duration
    db_elec.df[column][0] = first_val 
    db_elec.df[column].interpolate(method='time').plot(style='--')

db_elec.df['E_CHP'].plot()
db_elec.df['E_Imp'].plot()
db_elec.df['E_Exp'].plot()
plt.legend(['CHP', 'Imp', 'Exp'])

#replace_first('E_Exp')

#db_therm.df['E_CHP'].plot()
# db_therm.df['E_DHW'].plot()
# db_therm.df['E_SH'].plot()
# plt.legend(['CHP', 'DHW', 'SH'])




print('Start, {}'.format(db_elec.df[0:1].index[0]))
print('End, {}'.format(db_elec.df[-2:-1].index[0]))

elAbs = db_elec.df[-2:-1] - db_elec.df[0:1].values
print('E_el_CHP, {}, MWh'.format(round(elAbs['E_CHP'].values[0]/1000,2)))
print('E_el_Imp, {}, MWh'.format(round(elAbs['E_Imp'].values[0]/1000,2)))
print('E_el_Exp, {}, MWh'.format(round(elAbs['E_Exp'].values[0]/1000,2)))

thAbs = db_therm.df[-2:-1] - db_therm.df[0:1].values
print('E_th_CHP, {}, MWh'.format(round(thAbs['E_CHP'].values[0]/1000,2)))
print('E_th_Boi, {}, MWh'.format(round(thAbs['E_Boi'].values[0]/1000,2)))
print('E_th_DHW, {}, MWh'.format(round(thAbs['E_DHW'].values[0]/1000,2)))
print('E_th_SH, {}, MWh'.format(round(thAbs['E_SH'].values[0]/1000,2)))

gasAbs = db_gas.df[-2:-1] - db_gas.df[0:1].values
print('E_gas_CHP, {}, MWh'.format(round(gasAbs['E_CHP'].values[0]/1000,2)))
print('E_gas_Boi, {}, MWh'.format(round(gasAbs['E_Boi'].values[0]/1000,2)))
print('V_gas_CHP, {}, m^3'.format(round(gasAbs['V_CHP'].values[0]/1000,2)))
print('V_gas_Boi, {}, m^3'.format(round(gasAbs['V_Boi'].values[0]/1000,2)))



#db_elec.df['P_Sol'].resample('H', how='mean').plot()
#db_elec.df['P_SolGluc'].resample('H', how='mean').plot()
#plt.legend(['Solar','SolarGluc', 'DHW'])
#db_elec.df['P_Dem'].resample('M', how='mean').plot()
#db_elec.df['P_CHP'].resample('M', how='mean').plot()


plt.ylabel('Energy in kWh')
plt.grid('on')
plt.savefig(str(plantNum) + '.pdf')
plt.show()
