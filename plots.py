import matplotlib.pyplot as plt
import numpy 
from math import floor
from scipy.optimize import curve_fit
from datetime import datetime
    
class HeatVsTemp():
    def __init__(self):        
        fig, self.axes = plt.subplots(nrows=1, ncols=1, figsize=(15, 8))
        self.fontsize = 14   
  
    def applyPlotSettings(self):        
        self.axes.set_ylim(0,10)
        self.axes.set_xlim(-5,30)                       
        self.axes.grid(True)        
        self.axes.set_title('Heat over ambient air temperature', fontsize=self.fontsize)
        self.axes.set_xlabel('Ambient temperature in deg C', fontsize=self.fontsize)
        self.axes.set_ylabel('Average heat demand in kW', fontsize=self.fontsize)
        self.axes.legend(['Sigmoid fit', 'Measured Data'], fontsize=self.fontsize)
                              
    def showPlot(self):
        plt.show()
            
    def saveFig(self, nameTrunc):
        now = datetime.now().strftime("%Y-%m-%d")
        filePath = nameTrunc + '_'+ now + '.pdf'        
        plt.savefig(filePath)        
               
    def feedDF(self, df):
        self.dataframe = df
        self.xSeries = self.dataframe['temp'].interpolate()
        self.ySeries = self.dataframe['heat'].interpolate()
        self.xmin = min(self.xSeries)        
                  
    def plotSeries(self):          
        self.dataframe.plot(ax=self.axes, kind='scatter', x='temp',y='heat', label='Measured data')
    
    def calcLinerarTrendline(self):        
        z = numpy.polyfit(self.xSeries, self.ySeries, 1)        
        self.trendline = numpy.poly1d(z)
        
    def plotLinearTrendLine(self):         
        self.calcLinerarTrendline()
        self.axes.plot(self.xSeries, self.trendline(self.xSeries),  label='trendline')
    
    def calcSigmoidTrendline(self):         
        popt, pcov = curve_fit(self.sigmoid1, self.xSeries , self.ySeries, [-5, 10, 2, 5])        
        x = numpy.linspace(self.xmin, 25, 100)
        y = self.sigmoid1(x, *popt)
        return x,y, popt 

    def sigmoid1(self, T, a, b, c, d):    
        y = a /(1+ (b/(T-(self.xmin-1)))**c) + d
        return y           
                
    def plotSigmoidFit(self):
        self.xmin = floor(min(self.xSeries))
        x,y, popt = self.calcSigmoidTrendline()
        self.axes.plot(x,y, label='sigmoidFit', linewidth = 5, color='r')        
        self.axes.text(11,8.5,r'$fit = \frac{a}{1+ (\frac{b}{T-T_0})^c} + d$', bbox=dict(facecolor='green', alpha=0.6), fontsize=self.fontsize)
        coefficientString = 'a=' + str(round(popt[0],2)) + '\;b=' + str(round(popt[1],2)) + '\;c=' + str(round(popt[2],2)) + '\;d=' + str(round(popt[3],2))
        self.axes.text(11,6.5, r'$' + coefficientString + '$', bbox=dict(facecolor='green', alpha=0.6), fontsize=self.fontsize)


class BarPlot():
    def __init__(self, ax):
        self.setAx(ax)        
        self.width = 0.3
        self.ax.grid('on')
        self.numberOfPlots = 0
    
    def getColor(self):
        #numberOfPlots = len(self.ax.containers)+len(self.ax.lines)        
        colors = ['b', 'r', 'g', 'm', 'y']        
        self.color = colors[self.numberOfPlots]
        self.numberOfPlots +=1

    def getOffset(self):
        numberOfPlots = len(self.ax.containers)+len(self.ax.lines)        
        if numberOfPlots == 0:            
            self.offset = -self.width/2
        else:
            self.offset = self.width/2        
            
    def setAx(self, ax):
        self.ax = ax
        
    def makeTitle(self, plantName, startDate, endDate):
        title = 'Plant number {pNum} ({pName}) \n From {sDay} to {eDay}'
        title = title.format(pNum = self.plantNumber, pName = plantName, sDay = startDate.date(), eDay = endDate.date())
        self.fig.suptitle(title, fontsize=20)
        
    def setLegend(self, legend):
        self.ax.legend(legend)
        
    def plot(self, yValues, legend=None):
        self.setupPlot()
        self.getOffset()
        self.getColor()
        self.ax.bar(self.xVals+self.offset, yValues, self.width, color=self.color, align='center', label=legend)
        self.ax.legend()       
    
    def plotStacked(self, yValues, yValuesStacked, legend=[None, None], yellowbottom=False):
        yValues = numpy.array(yValues)
        yValues[numpy.isnan(yValues)] = 0
        self.setupPlot()
        self.getOffset()       
        if yellowbottom:
            self.color='y'       
        else:
            self.getColor() 
        self.ax.bar(self.xVals+self.offset, yValues, self.width, color=self.color, align='center', label=legend[0])
        self.getColor()
        self.ax.bar(self.xVals+self.offset, yValuesStacked, self.width, color=self.color, bottom=yValues, align='center', label=legend[1])
        self.ax.legend()
    
    def annotate(self, prod, cons):
        self.ax.annotate('Production', xy=(0.85, prod*1.01), xytext=(0.2, prod*1.1), 
            bbox=dict(boxstyle="square", fc="w", alpha=0.9), 
            arrowprops=dict(facecolor='black', arrowstyle="-|>"))
               
        
        self.ax.annotate('Demand', xy=(1.15, cons*1.01), xytext=(1.3, cons*1.2),
            bbox=dict(boxstyle="square", fc="w", alpha=0.9),
            arrowprops=dict(facecolor='black', arrowstyle="-|>"))
        
    def plotTrippleStacked(self, yValues1, yValues2, yValues3, legend=[None, None, None]):
        yValues1 = numpy.array(yValues1)
        yValues1[numpy.isnan(yValues1)] = 0
        self.setupPlot()
        self.getOffset()        
        self.getColor() 
        self.ax.bar(self.xVals+self.offset, yValues1, self.width, color=self.color, align='center', label=legend[0])
        self.getColor()
        self.ax.bar(self.xVals+self.offset, yValues2, self.width, color=self.color, bottom=yValues1, align='center', label=legend[1])
        self.getColor()
        self.ax.bar(self.xVals+self.offset, yValues3, self.width, color=self.color, bottom=yValues1+yValues2, align='center', label=legend[2])
        self.ax.legend()
        
        
class MonthPlot(BarPlot):        
    def setupPlot(self):
        self.xVals = numpy.array(range(1,13))
        self.xticks = ['Jan','Feb','Mar','Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec']
        self.ax.set_xticks(self.xVals, minor=False)
        self.ax.set_xticklabels(self.xticks, fontdict=None, minor=False)
        self.ax.set_xlim(0, 13)
        self.ax.set_xlabel('Months')
        self.ax.set_ylabel('Average daily energy Demand in kWh')
    
class DayPlot(BarPlot):
    def setupPlot(self):
        self.xVals = numpy.array([1,2,3,4,5,6,7])
        self.xticks = ['Monday','Tuesday','Wednesday','Thursday', 'Friday', 'Saturday', 'Sunday']
        self.ax.set_xticks(self.xVals, minor=False)
        self.ax.set_xticklabels(self.xticks, fontdict=None, minor=False)
        self.ax.set_xlim(0, 8)
        self.ax.set_xlabel('Weekdays')
        self.ax.set_ylabel('Average daily energy Demand in kWh')
        
class HourPlot(BarPlot):
    
    def setupPlot(self):
        self.getColor()
        self.ax.set_xticks(range(0,24), minor=False)
        
    def plot(self, values):
        self.setupPlot()        
        self.ax.set_xlim(0, 23)
        self.ax.set_xlabel('Hours of the day')
        self.ax.set_ylabel('Average power in kW')        
        self.ax.plot(values, color=self.color)
        

class ThreePlotPage():
    
    def __init__(self):        
        self.fig, self.axes = plt.subplots(nrows=3, ncols=1, figsize=(15, 20))
            
    def makeTitle(self, plantNumber,plantName, startDate, endDate):
        title = 'Plant number {pNum} ({pName}) \n From {sDay} to {eDay}'
        title = title.format(pNum = plantNumber, pName = plantName, sDay = startDate.date(), eDay = endDate.date())
        self.fig.suptitle(title, fontsize=20) 
 
class SCPlot():    
    
    def hour(self, avgProd, avgDem):
        self.plotHour(avgProd, 'r')
        self.plotHour(avgDem, 'b')
        self.setLegend([0],['Production', 'Consumption'])        
        
    def day(self,avgProd, avgDem):
        self.plotDays(avgProd, 'r')
        self.plotDays(avgDem, 'b')
        self.setLegend([1],['Production', 'Consumption'])
            
    def month(self, avgProd, avgDem):
        self.plotMonths(avgDem, 'r')
        self.plotMonths(avgProd, 'b')
        self.setLegend([2],['Production', 'Consumption'])

    
class TSPlot():
    
    def __init__(self, plantNumber):
        self.plantNumber = plantNumber
        self.fig, self.ax = plt.subplots(nrows=1, ncols=1, figsize=(15, 20))        
        self.setPlot()
        
    def plotTS(self, timeseries):
        ax = self.ax
        ax.plot(timeseries.index, timeseries.values, '.')        
        
    def setPlot(self):
        self.ax.grid('on')
        self.ax.set_xlabel('Time')
        self.ax.set_ylabel('Demand in kW')
        
    def save(self, trunk):
        plotFileName = trunk + '_{plantNum}.pdf'.format(plantNum = self.plantNumber)
        plt.savefig(plotFileName)     
         