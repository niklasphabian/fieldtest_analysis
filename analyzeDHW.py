'''
Created on Jul 8, 2016

@author: EIFERKITEDU\griessbaum
'''

import database
import datetime

startDate = datetime.datetime(2015,1,1)
endDate = datetime.datetime(2015,12,31)                

db = database.Thermal(7)
db.setStart(startDate)
db.setEnd(endDate)

db.fetchData()
df = db.df

# Number of times in which heat demand is zero during the period of at least one hour after heat demand was greater zero
print(sum((df['P_DHW'].resample(rule='1h').mean() == 0 ) & (df['P_DHW'].resample(rule='1h').mean().diff()<0)))

