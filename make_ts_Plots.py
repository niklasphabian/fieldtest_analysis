import database
import datetime
import matplotlib.pyplot as plt

startDate = datetime.datetime(2016,8,1)
endDate = datetime.datetime(2016,8,5)


def plot_el():
    elDB = database.Electricity(2)
    #elDB = database.ElectricityMandelieu()
    elDB.setEnd(endDate)
    elDB.setStart(startDate)
    elDB.fetchData()
    elDB.df['P_CHP'].resample('H').mean().plot()
    

def plot_th():
    thDB = database.Thermal(1)
    #thDB = database.ThermalMandelieu()
    thDB.setStart(startDate)
    thDB.setEnd(endDate)
    thDB.fetchData()
    print(thDB.df)
    thDB.df['P_CHP'].resample('H').mean().plot()
    #thDB.df['P_Boi'].resample('H', how='mean').plot()
    
    
#plot_th()
plot_el()



plt.legend()
plt.grid('on')
plt.xlabel('Time')
plt.ylabel('Power in kW')
plt.show()