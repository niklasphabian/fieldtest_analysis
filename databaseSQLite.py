class SQLDB:
    def __init__(self):
        self.makeCon()
    
    def makeCon(self):
        self.conn = sqlite3.connect('maisonEvolutive.db')
        self.cursor = self.conn.cursor()
        
    def setStart(self, startDate):
        self.start = startDate
    
    def setEnd(self, endDate):
        self.end = endDate
        
    def getTS(self, column):
        query = 'SELECT timestamp, {column} AS Q \
                 FROM demand \
                 WHERE timestamp >= ? AND timestamp < ? \
                 AND {column} < 100000 \
                 order by timestamp;'.format(column=column)        
        self.cursor.execute(query, (self.start, self.end,))
        data = self.cursor.fetchall()
        return self.convertDataToPandas(data)
        
    def getSHTS(self):
        column = 'sh'
        return self.getTS(column)        
        
    def getDHWTS(self):
        column = 'dhw'
        return self.getTS(column)
    
    def convertDataToPandas(self, data):
        timestamp = []
        heat = []
        for row in data:
            timestamp.append(datetime.datetime.strptime(row[0], '%Y-%m-%d %H:%M:%S'))                        
            heat.append(row[1])             
        timeseries = pandas.TimeSeries(heat, timestamp, name = 'HeatDemand')
        timeseries /=1000        
        return timeseries