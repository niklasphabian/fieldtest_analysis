import matplotlib as mpl
import matplotlib.pyplot as plt

fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(20, 8))
cm = ['r', 'b', 'r','b']
cmap = mpl.colors.ListedColormap(cm)
cmap.set_over(color='k',alpha=None)
cmap.set_under(color='k',alpha=None)
#cmap.set_bad(color='k',alpha=None)


bounds = [1,2,3,4,5]        

norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
cb2 = mpl.colorbar.ColorbarBase(ax, cmap=cmap,
                                     norm=norm,
                                     # to use 'extend', you must
                                     # specify two extra boundaries:
                                     boundaries=[0.9]+bounds+[6],
                                     extend='both',
                                     ticks=bounds, # optional
                                     spacing='proportional',
                                     orientation='horizontal')

plt.show()
#cb2.set_label('Availibility of Box # ' +str(self.series.boxnr))
#ticks = self.makeTicks()
#column_labels = ticks        
#ax.set_xticklabels(column_labels, minor=False,rotation=90)        
#ax.tick_params(labelsize=8)