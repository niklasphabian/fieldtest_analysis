import sqlite3
import csv
import datetime

conn = sqlite3.connect('maisonEvolutive.db')
cursor = conn.cursor()
deltaT = datetime.timedelta(minutes=10)

with open('CdC_Maison_Evolutive.csv') as inFile:
    reader = csv.reader(inFile)
    reader.next()    
    timeStamp = datetime.datetime(2014,1,1) 
    for row in reader:        
        sh = row[2]
        dhw = row[3]  
        cursor.execute('INSERT INTO demand VALUES(?,?,?)', (timeStamp, sh, dhw))
        timeStamp = timeStamp + deltaT 

conn.commit()
conn.close()