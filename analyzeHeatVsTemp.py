import database
import plots

heatDB = database.HeatVsTemp(5)
startDate = '2013-03-01'
endDate = '2015-10-01'

heatDB.setStart(startDate)
heatDB.setEnd(endDate)
ts = heatDB.getHeatVsTemp()


tsResampled = ts.resample('4h', how='mean')
tsResampled = tsResampled[tsResampled['temp'] < 25]
#tsResampled = tsResampled[tsResampled['temp'] != 0]
tsResampled = tsResampled[tsResampled['heat']>0]

myPlot = plots.HeatVsTemp()
myPlot.feedDF(tsResampled)
myPlot.plotSeries()
myPlot.plotSigmoidFit()
myPlot.applyPlotSettings()

myPlot.saveFig('HeatVsTemp')
myPlot.showPlot()


