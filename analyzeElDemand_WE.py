import database
import matplotlib.dates as mdates
import datetime
import matplotlib.pyplot as plt
import matplotlib.dates as dates
import pytz

met = pytz.timezone('MET')

db_elec = database.Electricity(7)
startDate = datetime.datetime(2013,9,23,0,0,0)
endDate = datetime.datetime(2015,7,30,0,0)
db_elec.setStart(startDate)
db_elec.setEnd(endDate)

fig, ax = plt.subplots()

fig.suptitle('total el demand: {dem} kWh, total el production: {prod} kWh'.format(dem=db_elec.totalDemand(), prod=db_elec.totalProduction()))

db_elec.getTS()

#ax.plot_date(db_elec.elDem.index, db_elec.elDem, '-')
db_elec.elDem.plot(axes=ax, color='g')
db_elec.elProd.plot(axes=ax, color='r')

ax.xaxis.set_major_locator(dates.DayLocator(tz=met))
ax.xaxis.set_major_formatter(dates.DateFormatter('\n\n %Y-%m-%d', tz=met))

ax.xaxis.set_minor_locator(dates.HourLocator(tz=met, interval=2))
ax.xaxis.set_minor_formatter(dates.DateFormatter('\n%H', tz=met))
ax.xaxis.grid(True, which="minor")
ax.yaxis.grid(True, which="major")
ax.set_ylabel('Electric power in kW')
ax.legend(['Demand', 'CHP production'], )


bbox_props = dict(boxstyle="round", fc="w", ec="0.5", alpha=0.9)
xAnnote = mdates.date2num(datetime.datetime(2015,9,6,1,1))
ax.annotate('Sauna \n (Yuiiii)', (xAnnote, 7), xytext=(30, 30), textcoords='offset points', arrowprops=dict(arrowstyle='-|>'), ha='center', bbox=bbox_props)

xAnnote = mdates.date2num(datetime.datetime(2015,9,3,21,0))
ax.annotate('Boiling pasta', (xAnnote, 3), xytext=(30, 30), textcoords='offset points', arrowprops=dict(arrowstyle='-|>'), ha='center', bbox=bbox_props)

xAnnote = mdates.date2num(datetime.datetime(2015,9,4,7,0))
ax.annotate('Making coffee \n (damn thats early)', (xAnnote,2), xytext=(30, 30), textcoords='offset points', arrowprops=dict(arrowstyle='-|>'), ha='center', bbox=bbox_props)

xAnnote = mdates.date2num(datetime.datetime(2015,9,4,20,30))
ax.annotate('Frying cheese + TV', (xAnnote, 4), xytext=(30, 30), textcoords='offset points', arrowprops=dict(arrowstyle='-|>'), ha='center', bbox=bbox_props)

xAnnote = mdates.date2num(datetime.datetime(2015,9,5,3,0))
ax.annotate('Alex goes to bed', (xAnnote, 0.5), xytext=(30, 30), textcoords='offset points', arrowprops=dict(arrowstyle='-|>'), ha='center', bbox=bbox_props)

xAnnote = mdates.date2num(datetime.datetime(2015,9,5,12,30))
ax.annotate('Coffe/Bread', (xAnnote, 2), xytext=(30, 30), textcoords='offset points', arrowprops=dict(arrowstyle='-|>'), ha='center', bbox=bbox_props)

xAnnote = mdates.date2num(datetime.datetime(2015,9,5,21,30))
ax.annotate('Brittney!!! \n (stereo)', (xAnnote, 0.5), xytext=(30, 30), textcoords='offset points', arrowprops=dict(arrowstyle='-|>'), ha='center', bbox=bbox_props)

xAnnote = mdates.date2num(datetime.datetime(2015,9,3,4,30))
ax.annotate('Fridge+Freezer noise', (xAnnote, .2), xytext=(30, 30), textcoords='offset points', arrowprops=dict(arrowstyle='-|>'), ha='center', bbox=bbox_props)

xAnnote = mdates.date2num(datetime.datetime(2015,9,6,9,0))
ax.annotate('Timed dishwasher \n waking up Alexandru', (xAnnote, 3), xytext=(30, 30), textcoords='offset points', arrowprops=dict(arrowstyle='-|>'), ha='center', bbox=bbox_props)

xAnnote = mdates.date2num(datetime.datetime(2015,9,6,19,30))
ax.annotate('Vacuum \n (thank you, Romania)',  (xAnnote, 2), xytext=(30, 30), textcoords='offset points', arrowprops=dict(arrowstyle='-|>'), ha='center', bbox=bbox_props)


ax.set_ylim(0,1)

plt.show()    
    