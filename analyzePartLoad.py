'''
Created on Apr 22, 2016

@author: EIFERKITEDU\griessbaum
'''

import database
import datetime
import pytz
import matplotlib.pyplot as plt

met = pytz.timezone('MET')

#db_elec = database.Electricity(1)
db_elec = database.ElectricityMandelieu()
db_therm = database.ThermalMandelieu()
startDate = datetime.datetime(2016,2,10)
endDate = datetime.datetime(2016,2,11)
db_elec.setStart(startDate)
db_elec.setEnd(endDate)
db_therm.setStart(startDate)
db_therm.setEnd(endDate)
db_elec.fetchData()
db_therm.fetchData()

fig, ax = plt.subplots()
df_elec = db_elec.df
df_therm = db_therm.df

#df['P_CHP'][df['P_CHP']<1].plot(axes=ax, color='r')

df_elec['P_CHP'][df_elec['P_CHP']<1].plot(axes=ax, color='r')
df_therm['P_CHP'].plot(axes=ax, color='b')

plt.grid('on')
plt.show()    
