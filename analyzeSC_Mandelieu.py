import database
import datetime
import plots
import matplotlib.pyplot as plt

class SCAnalyzer():
    def __init__(self, plantNumber):
        self.plantNumber = plantNumber
        self.db = database.ElectricityMandelieu()
        self.plantName = self.db.plantName
        startDate = datetime.datetime(2016,1,1)
        endDate = datetime.datetime(2016,,28)
        self.db.setStart(startDate)
        self.db.setEnd(endDate)
        self.db.fetchData()
        self.canvas = plots.ThreePlotPage()
        self.canvas.makeTitle(self.plantNumber, self.plantName, startDate, endDate)                
                
    def plotHour(self):
        avgProd = []
        avgExp = []        
        for hour in range(0,24):
            avgProd.append(self.db.mean_power_by_hour_chp(hour))
            avgExp.append(self.db.mean_power_by_hour_export(hour))        
        hourPlot = plots.HourPlot(self.canvas.axes[0])
        hourPlot.plot(avgProd)
        hourPlot.plot(avgExp)
        hourPlot.setLegend(['El. Production', 'El. Export'])
                                
    def plotDays(self):
        avgProd = []
        avgExp = []        
        days = [0,1,2,3,4,5,6]
        for day in days:
            avgProd.append(self.db.mean_power_by_day_chp(day)*24)           
            avgExp.append(self.db.mean_power_by_day_export(day)*24)
        dayPlot = plots.DayPlot(self.canvas.axes[1])
        dayPlot.plot(avgProd,legend='El. Production')
        dayPlot.plot(avgExp, legend='El. Export')        

    def plotMonths(self):
        avgProd = []
        avgExp = [] 
        months = range(1,13)
        for month in months:            
            avgProd.append(self.db.mean_power_by_month_chp(month)*24)           
            avgExp.append(self.db.mean_power_by_month_export(month)*24)
        monthPlot = plots.MonthPlot(self.canvas.axes[2])        
        monthPlot.plot(avgProd, legend='El. Production')
        monthPlot.plot(avgExp, legend='El. Export')
                
    def saveFigs(self):
        plotFileName = 'SC_{plantNum}.pdf'.format(plantNum = self.plantNumber)
        plt.savefig(plotFileName)
      
def plotInstallation(inst):
    myAnalyzer = SCAnalyzer(inst)
    myAnalyzer.plotHour()    
    myAnalyzer.plotDays()
    myAnalyzer.plotMonths()
    #plt.show()
    myAnalyzer.saveFigs()
    
plotInstallation(11)
    

