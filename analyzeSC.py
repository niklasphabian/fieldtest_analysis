import database
import datetime
import plots
import matplotlib.pyplot as plt

class SCAnalyzer():
    def __init__(self, plantNumber):
        self.plantNumber = plantNumber  
        self.db = database.Electricity(self.plantNumber)
        self.plantName = self.db.plantName
        startDate = datetime.datetime(2016,1,1)
        endDate = datetime.datetime(2017,1,1)
        self.db.setStart(startDate)
        self.db.setEnd(endDate)
        self.db.fetchData()
        self.canvas = plots.ThreePlotPage()
        self.canvas.makeTitle(self.plantNumber, self.plantName, startDate, endDate)                
                
    def plotHour(self):
        avgProd = []
        avgDem = []
        for hour in range(0,24):
            avgProd.append(self.db.mean_power_by_hour_chp(hour))
            avgDem.append(self.db.mean_power_by_hour_demand(hour))        
        hourPlot = plots.HourPlot(self.canvas.axes[0])
        hourPlot.plot(avgProd)
        hourPlot.plot(avgDem)
        hourPlot.setLegend(['El. Production', 'El. Demand'])
                                
    def plotDays(self):
        avgImp = []
        avgExp = []
        avgSC = []
        days = [0,1,2,3,4,5,6]
        for day in days:            
            avgImp.append(self.db.mean_power_by_day_import(day)*24)
            avgExp.append(self.db.mean_power_by_day_export(day)*24)
            avgSC.append(self.db.mean_power_by_day_selfcons(day)*24)
        dayPlot = plots.DayPlot(self.canvas.axes[1])
        dayPlot.plotStacked(avgSC, avgExp, legend=['El. Self-Consumption','El. Export'], yellowbottom=True)
        dayPlot.plotStacked(avgSC, avgImp, legend=[None, 'El. Import'], yellowbottom=True)        
        dayPlot.annotate(avgSC[0]+avgExp[0], avgSC[0]+avgImp[0])

    def plotMonths(self):
        avgImp = []
        avgExp = []
        avgSC = []
        days = range(1,13)
        for day in days:            
            avgImp.append(self.db.mean_power_by_month_import(day)*24)
            avgExp.append(self.db.mean_power_by_month_export(day)*24)
            avgSC.append(self.db.mean_power_by_month_selfcons(day)*24)
        monthPlot = plots.MonthPlot(self.canvas.axes[2])        
        monthPlot.plotStacked(avgSC, avgExp, legend=['El. Self-Consumption','El. Export'], yellowbottom=True)
        monthPlot.plotStacked(avgSC, avgImp, legend=[None, 'El. Import'], yellowbottom=True)
        monthPlot.annotate(avgSC[0]+avgExp[0], avgSC[0]+avgImp[0])        
        
    def saveFigs(self):
        plotFileName = 'SC_{plantNum}.pdf'.format(plantNum = self.plantNumber)
        plt.savefig(plotFileName)
      
def plotInstallation(inst):
    myAnalyzer = SCAnalyzer(inst)
    myAnalyzer.plotHour()    
    myAnalyzer.plotDays()
    myAnalyzer.plotMonths()
    myAnalyzer.saveFigs()
    
plotInstallation(8)

    

